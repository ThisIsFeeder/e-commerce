@extends('master')

@section('content')

<!--Main layout-->
<main>
  <div class="container">
    <!--Section: Block Content-->
    <section class="mt-5 mb-4">
      <!--Row-->
      <div class="row">
        <!--Left-->
        <div class="col-lg-3">
          <div class="border rounded p-4 single category mb-4">
            <h3 class="side-title">Categories</h3>
            <ul class="list-unstyled">
              <li><a href="{{route('products.category', ['men'])}}">Men <span class="float-right">13</span></a>
              </li>
              <li><a href="{{route('products.category', ['women'])}}" title="">Women <span
                    class="float-right">13</span></a>
              </li>
              <li><a href="{{route('products.category', ['boys'])}}" title="">Boys <span
                    class="float-right">13</span></a>
              </li>
              <li><a href="{{route('products.category', ['girls'])}}" title="">Girls <span
                    class="float-right">13</span></a>
              </li>
              <li><a href="{{route('products.category', ['accessories'])}}" title="">Accessories <span
                    class="float-right">13</span></a></li>
            </ul>
          </div>
          <div class="border p-4 rounded">

            {{-- Range Slider --}}
            <div class="mb-4">
              <h3 class="mb-3 h6 text-uppercase text-black d-block">Filter by Price</h3>
              <div id="slider-range" class="border-primary"></div>
              <input type="text" name="text" id="amount" class="form-control border-0 pl-0 bg-transparent"
                disabled="" />
            </div>

            {{-- Size --}}
            <div class="mb-4">
              <h3 class="mb-3 h6 text-uppercase text-black d-block">Size</h3>
              <label for="s_sm" class="d-flex">
                <input type="checkbox" id="s_sm" class="mr-2 mt-1"> <span class="text-black font-weight-normal">Small
                  (2,319)</span>
              </label>
              <label for="s_md" class="d-flex">
                <input type="checkbox" id="s_md" class="mr-2 mt-1"> <span class="text-black font-weight-normal">Medium
                  (1,282)</span>
              </label>
              <label for="s_lg" class="d-flex">
                <input type="checkbox" id="s_lg" class="mr-2 mt-1"> <span class="text-black font-weight-normal">Large
                  (1,392)</span>
              </label>
            </div>

            {{-- Colors --}}
            <div class="mb-4">
              <h3 class="mb-3 h6 text-uppercase text-black d-block">Color</h3>
              <a href="#" class="d-flex color-item align-items-center">
                <span class="bg-danger color d-inline-block rounded-circle mr-2"></span> <span class="text-black">Red
                  (2,429)</span>
              </a>
              <a href="#" class="d-flex color-item align-items-center">
                <span class="bg-success color d-inline-block rounded-circle mr-2"></span> <span class="text-black">Green
                  (2,298)</span>
              </a>
              <a href="#" class="d-flex color-item align-items-center">
                <span class="bg-info color d-inline-block rounded-circle mr-2"></span> <span class="text-black">Blue
                  (1,075)</span>
              </a>
              <a href="#" class="d-flex color-item align-items-center">
                <span class="bg-primary color d-inline-block rounded-circle mr-2"></span> <span
                  class="text-black">Purple (1,075)</span>
              </a>
            </div>

          </div>
        </div>
        <!--End Left-->

        <!--Right-->
        <div class="col-lg-9">
          <!-- Header -->
          <div class="row align">
            <div class="col-md-12 mb-3">
              <div class="float-md-left">

                <button class="btn btn-outline-primary" onclick="window.location='{{ route('products.category') }}'"><i
                    class="fas fa-shopping-bag mr-2"></i>{{__('Shop All')}}</button>
              </div>
              <div class="d-flex">
                <div class="dropdown mr-1 ml-md-auto">
                  <button type="button" class="btn btn-white btn-sm dropdown-toggle px-4" id="dropdownMenuOffset"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Latest
                  </button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuOffset">
                    <a class="dropdown-item" href="#">Men</a>
                    <a class="dropdown-item" href="#">Women</a>
                    <a class="dropdown-item" href="#">Boys</a>
                    <a class="dropdown-item" href="#">Girls</a>
                    <a class="dropdown-item" href="#">Accessories</a>
                  </div>
                </div>
                <div class="btn-group">
                  <button type="button" class="btn btn-white btn-sm dropdown-toggle px-4" id="dropdownMenuReference"
                    data-toggle="dropdown">Short By</button>
                  <div class="dropdown-menu" aria-labelledby="dropdownMenuReference">
                    <a class="dropdown-item" href="#">Relevance</a>
                    <a class="dropdown-item" href="#">Name, A to Z</a>
                    <a class="dropdown-item" href="#">Name, Z to A</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Price, low to high</a>
                    <a class="dropdown-item" href="#">Price, high to low</a>
                  </div>
                </div>
              </div>
            </div>
            <!--End header-->
            <hr class="mb-4">
            <div class="card-body">
              <h3 class="mb-4">SHOES</h3>
              <!-- Tittle -->
              <div class="container">
                <div class="row" id="ads">
                  <!-- Category Card -->

                  @for ($index = 0; $index <3; $index++) <div class="col-md-4">
                    <div class="card rounded">
                      <div class="card-image">
                        <img class="img-fluid"
                          src="{{asset($products[$index]->images[0]->path ?? $defaultProductImagePath)}}"
                          alt="{{$products[$index]->name}}" />
                      </div>
                      <div class="card-body text-center">
                        <div class="ad-title m-auto">
                          <h5>{{$products[$index]->name}}</h5>
                          <a class="ad-btn"
                            href="{{route('products.show', $products[$index])}}">{{$products[$index]->productDetail()->min('price')}}</a>
                        </div>
                      </div>
                    </div>
                </div>
                @endfor

                <a class="ads-btn" href="{{route('products.category', ['men'])}}">See More Item</a>
              </div>
            </div>
          </div>
          <!--End header-->
          <!--End Shoes-->

          <hr class="mb-4">
          <!-- Bag -->
          <div class="card-body">
            <h3 class="mb-4">BAGS</h3>
            <!-- Tittle -->
            <div class="container">
              <div class="row" id="ads">
                <!-- Category Card -->

                @for ($index = 3; $index <6; $index++) 
                <div class="col-md-4">
                  <div class="card rounded">
                    <div class="card-image">
                      <img class="img-fluid"
                        src="{{asset($products[$index]->images[0]->path ?? $defaultProductImagePath)}}"
                        alt="{{$products[$index]->name}}" />
                    </div>
                    <div class="card-body text-center">
                      <div class="ad-title m-auto">
                        <h5>{{$products[$index]->name}}</h5>
                        <a class="ad-btn" href="{{route('products.show', $products[$index])}}">{{$products[$index]->productDetail()->min('price')}}</a>
                      </div>
                    </div>
                  </div>
                </div>
                @endfor

                <a class="ads-btn" href="{{route('products.category', ['women'])}}">See More Item</a>
              </div>
            </div>
          </div>
          <!-- End Bags -->
          <hr class="mb-4">
          <!-- T-Shirt -->
          <div class="card-body">
            <h3 class="mb-4">{{__('T-Shirt')}}</h3>
            <!-- Tittle -->
            <div class="container">
              <div class="row" id="ads">
                <!-- Category Card -->

                @for ($index = 6; $index <9; $index++) 
                <div class="col-md-4">
                  <div class="card rounded">
                    <div class="card-image">
                      <img class="img-fluid"
                        src="{{asset($products[$index]->images[0]->path ?? $defaultProductImagePath)}}"
                        alt="{{$products[$index]->name}}" />
                    </div>
                    <div class="card-body text-center">
                      <div class="ad-title m-auto">
                        <h5>{{$products[$index]->name}}</h5>
                        <a class="ad-btn" href="{{route('products.show', $products[$index])}}">{{$products[$index]->productDetail()->min('price')}}</a>
                      </div>
                    </div>
                  </div>
                </div>
                @endfor

                <a class="ads-btn" href="{{route('products.category', ['boys'])}}">See More Item</a>
              </div>
            </div>
          </div>
          <!-- End T-Shirt -->

          <hr class="mb-4">
          <!-- Scarf -->
          <div class="card-body">
            <h3 class="mb-4">{{__('Scarf')}}</h3>
            <!-- Tittle -->
            <div class="container">
              <div class="row" id="ads">
                <!-- Category Card -->

                @for ($index = 9; $index <12; $index++) 
                <div class="col-md-4">
                  <div class="card rounded">
                    <div class="card-image">
                      <img class="img-fluid" src="{{asset($products[$index]->images[0]->path ?? $defaultProductImagePath)}}"
                        alt="{{$products[$index]->name}}" />
                    </div>
                    <div class="card-body text-center">
                      <div class="ad-title m-auto">
                        <h5>{{$products[$index]->name}}</h5>
                        <a class="ad-btn" href="{{route('products.show', $products[$index])}}">{{$products[$index]->productDetail()->min('price')}}</a>
                      </div>
                    </div>
                  </div>
                </div>
                @endfor
                <a class="ads-btn" href="{{route('products.category', ['girls'])}}">See More Item</a>
              </div>
            </div>
          </div>
          <!-- End Scarf -->
        </div>
      </div>
      <!--Section: Block Content-->
      <!--Row-->
    </section>
    <!--End Section-->
  </div>
</main>
@endsection
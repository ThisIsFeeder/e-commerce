<?php

use Illuminate\Database\Seeder;
use App\Models\ProductCodeType;
class ProductCodeTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ProductCodeType::class, 10)->create();
    }
}

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductDetail;
use Faker\Generator as Faker;

$factory->define(ProductDetail::class, function (Faker $faker) {
    return [

        'product_id'=>$faker->randomDigitNot(0),
        'product_detail_code'=>$faker->randomNumber,
        'parent_id'=>$faker->randomDigitNot(0),
        'option_id'=>$faker->randomDigitNot(0),
        'option_value_id'=>$faker->randomDigitNot(0),
        'price'=>$faker->randomFloat(5, 2, 100),

    ];
});

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OptionValue extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'description', 'option_id'];

    public function option()
    {
        return $this->belongsTo('App\Models\Option');
    }
}

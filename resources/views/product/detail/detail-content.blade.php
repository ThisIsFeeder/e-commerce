<!-- Content -->
<section>
    <div class="content-wrapper col-md-6 mx-auto pt-3 px-4 mt-3 bg-white">

        <div class="row mt-4">
            <div class="col-md-6">
                <div
                    style="background: url('{{asset($product->images[0]->path ?? $defaultPath)}}'); background-size: cover; height: 100%">
                </div>
            </div>
            <div class="col-md-6">
                <div class="upper">
                    <h4 class="font-weight-bold text-uppercase mb-0">{{$product->name}}</h4>
                    <p class="text-secondary">{{__('Free Delivery')}}</p>
                    <h4 class="my-3">${{$product->productDetail->min('price')}}-${{$product->productDetail->max('price')}}</h4>
                    <div class="d-flex mx-md-n1 mt-3">
                        <div class="btn btn-info mx-1">S</div>
                        <div class="btn btn-outline-info mx-1">M</div>
                        <div class="btn btn-outline-info mx-1">L</div>
                    </div>
                    <div class="qty_btn_grp d-flex mt-3 mx-md-n1">
                        <p class="font-weight-bold d-block align-self-center pr-3">Quantity:</p>
                        <div>
                            <p class="badge badge-info mx-1 p-2">-</p>
                            <div class="btn btn-outline-info">01</div>
                            <p class="badge badge-info mx-1 p-2">+</p>
                        </div>
                    </div>
                </div>
                <div class="lower mt-4">
                    <div class=" btn btn-info w-100 mt-2">Buy Now</div>
                    <div class="btn btn-outline-info w-100 mt-2" onclick="location.href='{{route('cart.index')}}'">Add to cart</div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-md-12">
                <p class="lead font-weight-bold">Description:</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
                    et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                    aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum
                    dolore eu
                    fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum.</p>
            </div>
        </div>
    </div>
</section>

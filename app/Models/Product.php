<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use SoftDeletes;
    protected $fillable = ['product_code', 'product_code_type_id', 'name', 'description'];

    public function categories() {
        return $this->morphToMany('App\Models\Category', "categorizable");
    }

    public function productCodeType() {
        return $this->belongsTo('App\Models\ProductCodeType');
    }

    public function images() {
        return $this->morphToMany('App\Models\Image', "imageable");
    }

    public function productDetail(){
        return $this->hasMany('App\Models\ProductDetail');
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);

        $this->call(ProductTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(ProductCodeTypeTableSeeder::class);
        $this->call(OptionTableSeeder::class);
        $this->call(OptionValueTableSeeder::class);
        $this->call(ProductDetailTableSeeder::class);
        $this->call(ImageTableSeeder::class);
        $this->call(ImageTypeTableSeeder::class);
        // $this->call(OrderTableSeeder::class);
        $this->call(CartTableSeeder::class);

    }
}

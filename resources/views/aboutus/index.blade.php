@extends('master')
@section('content')
<div class="row justify-content-center mb-5">
  <div class="col-md-7 site-section-heading text-center pt-4">
    <div class="block-38 text-center">
      <div class="block-38-img">
        <hr>
        <h2>ORM / Web services</h2>
        <hr>
        <div class="block-38-header">
          <img src="images/photo_2017-08-18_08-55-49.jpg" alt="Image placeholder" class="mb-4">
          <h3 class="block-38-heading h4">Lypengleang Seang</h3>
          <p class="block-38-subheading">Instructor</p>
          <hr>
        </div>
        <h2>BiTribie Team</h2>
        <hr>
      </div>
    </div>

    <div class="row">

      <div class="col-md-6 col-lg-4 col-sm-12">
        <div class="block-38 text-center">
          <div class="block-38-img">
            <div class="block-38-header">
              <img src="images/photo_2020-03-02_16-09-28.jpg" alt="Image placeholder" class="mb-4">
              <h3 class="block-38-heading h4">Darom <br /> Sovannareach</h3>
              <p class="block-38-subheading">Team Leader</p>
            </div>

          </div>
        </div>
      </div>

      <div class="col-md-6 col-lg-4 col-sm-12">
        <div class="block-38 text-center">
          <div class="block-38-img">
            <div class="block-38-header">
              <img src="images/photo_2018-02-11_10-41-18.jpg" alt="Image placeholder" class="mb-4">
              <h3 class="block-38-heading h4">Ros <br /> Sovanndevit</h3>
              <p class="block-38-subheading">Team vice-leader</p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-lg-4 col-sm-12">
        <div class="block-38 text-center">
          <div class="block-38-img">
            <div class="block-38-header">
              <img src="images/photo_2020-03-16_11-13-25.jpg" alt="Image placeholder" class="mb-4">
              <h3 class="block-38-heading h4">Chhum <br /> Sengchhun</h3>
              <p class="block-38-subheading">Team member</p>
            </div>

          </div>
        </div>
      </div>

      <div class="col-md-6 col-lg-4 col-sm-12">
        <div class="block-38 text-center">
          <div class="block-38-img">
            <div class="block-38-header">
              <img src="images/photo_2020-01-12_20-37-30.jpg" alt="Image placeholder" class="mb-4">
              <h3 class="block-38-heading h4">Nom <br /> Sokheng</h3>
              <p class="block-38-subheading">Team member</p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-lg-4 col-sm-12">
        <div class="block-38 text-center">
          <div class="block-38-img">
            <div class="block-38-header">
              <img src="images/photo_2019-01-02_15-00-08.jpg" alt="Image placeholder" class="mb-4">
              <h3 class="block-38-heading h4">Theng <br /> Mengkheang</h3>
              <p class="block-38-subheading">Team member</p>
            </div>
          </div>
        </div>
      </div>

      <div class="col-md-6 col-lg-4 col-sm-12">
        <div class="block-38 text-center">
          <div class="block-38-img">
            <div class="block-38-header">
              <img src="images/photo_2018-12-07_10-22-11.jpg" alt="Image placeholder" class="mb-4">
              <h3 class="block-38-heading h4">Lum <br /> Seyha</h3>
              <p class="block-38-subheading">Team member</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
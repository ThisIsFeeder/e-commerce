@extends('master')

@section('content')
<div class="site-section">
	<div class="container h-100">
		<div class="row justify-content-md-center h-100">
			<div class="card-wrapper col-md-5">
				<div class="card card-primary card-outline">
					<div class="card-body box-profile">
						<div class="text-center">
							@if(count(Auth::user()->images)>0)
							<img class="profile-user-img img-fluid img-circle" style="width:100px; height: 100px;"
								src="{{ asset(Auth::user()->images[0]->path) }}" alt="User profile picture">
							@else
							<img class="profile-user-img img-fluid img-circle" style="width:100px; height: 100px;" src="../../dist/img/user4-128x128.jpg"
								alt="User profile picture">
							@endif
						</div>

						<h3 class="profile-username text-center">{{ Auth::user()->name }}</h3>

						<ul class="list-group list-group-unbordered mb-3">
							<li class="list-group-item">
								<b>{{__('Email')}}</b> <a class="float-right">{{ Auth::user()->email }}</a>
							</li>
							<li class="list-group-item">
								<b>{{__('Phone Number')}}</b> <a class="float-right">{{ Auth::user()->phone }}</a>
							</li>
						</ul>

						<button onclick="location.href='{{route('profile.edit', Auth::user()->id)}}'"
							class="btn btn-primary btn-block"><b>{{__('Edit')}}</b></button>
						<button onclick="location.href='{{route('profile.changePassword', Auth::user()->id)}}'"
							class="btn btn-outline-primary btn-block"><b>{{__('Change Password')}}</b></button>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
@extends('master')

@section('content')
<div class="site-section">
  <div class="container h-100">
    <div class="row justify-content-md-center h-100">
      <div class="card-wrapper col-md-5">
        <div class="card card-primary card-outline">
          <div class="card-body box-profile">
            <div class="text-center">
              <img class="profile-user-img img-fluid img-circle" src="../dist/img/user1-128x128.jpg"
                alt="User profile picture">
            </div>

            <h3 class="profile-username text-center">BiTribie Store</h3>

            <p class="text-muted text-center">St 233, Chamkamon Phnom Penh</p>

            <ul class="list-group list-group-unbordered mb-3">
              <li class="list-group-item">
                <b>Facebook</b> <a class="float-right">BiTribie Store</a>
              </li>
              <li class="list-group-item">
                <b>Email</b> <a class="float-right">admin@bitribie.com</a>
              </li>
              <li class="list-group-item">
                <b>Phone Number</b> <a class="float-right">010 222 333</a>
              </li>
            </ul>
          </div>
          <!-- /.card-body -->
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 11, 2020 at 05:40 PM
-- Server version: 10.1.38-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laravel`
--
CREATE DATABASE IF NOT EXISTS `laravel` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `laravel`;

-- --------------------------------------------------------

--
-- Table structure for table `addcarts`
--

CREATE TABLE IF NOT EXISTS `addcarts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `quantity` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cartables`
--

CREATE TABLE IF NOT EXISTS `cartables` (
  `cart_id` int(11) NOT NULL,
  `cartable_id` int(11) NOT NULL,
  `cartable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cartables`
--

INSERT INTO `cartables` (`cart_id`, `cartable_id`, `cartable_type`) VALUES
(1, 1, 'App\\Models\\User'),
(2, 1, 'App\\Models\\User');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE IF NOT EXISTS `carts` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `quantity` int(11) NOT NULL,
  `product_detail_id` int(11) NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `quantity`, `product_detail_id`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 2, 'Vitae beatae ut nobis reiciendis voluptate debitis modi. Repellendus ab et atque sunt voluptatum. Ut amet sit distinctio ad itaque.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(2, 8, 7, 'In voluptatibus molestiae quos et. Saepe velit qui porro ut velit ut. Illo possimus non cum autem dolorem. Quis tempora officiis quae et neque veritatis.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(3, 1, 3, 'Corporis quia dicta et cumque amet. Itaque sit repellat quia rem voluptas.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(4, 6, 3, 'Tempore ut eos est omnis doloremque sed assumenda. Illo ut numquam modi nam. Praesentium est error ullam adipisci adipisci sunt sunt.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(5, 7, 7, 'Magnam non et laudantium. Quam debitis voluptates architecto autem nam perspiciatis. Magnam maxime dignissimos est sapiente laborum et expedita. Corporis velit modi facilis dolor qui.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(6, 4, 4, 'Sunt laudantium placeat quia voluptatum amet fuga sint. Omnis ut tenetur cum quo animi quidem. Sit id porro error assumenda.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(7, 5, 6, 'Ipsa nulla velit numquam velit. Et quis est occaecati maxime sequi corrupti. Et voluptates et commodi qui ut adipisci vero. Impedit maxime illo ratione eaque. Voluptas explicabo quam et placeat vel est quo.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(8, 9, 7, 'Harum rerum numquam qui accusamus. Dolores enim dolorum qui quidem. Non optio adipisci autem. At est ipsa aut delectus dolorem tempora dignissimos at.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(9, 6, 7, 'Molestias saepe alias commodi. Architecto sed aut non ex. Recusandae nisi ratione dolor ab cupiditate tempora et rerum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(10, 3, 6, 'Officia nesciunt aut nam ad ratione totam. Cumque deleniti reprehenderit praesentium dolores ut. Nam autem ex ipsum. Aliquid praesentium eius error.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(11, 1, 4, 'Sint ab consectetur nam explicabo. Officiis veritatis voluptate eos. Illum eos aut et deserunt.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(12, 4, 5, 'Nihil esse officiis quo aut explicabo quia ipsa nostrum. At ad ab corporis eveniet quia et. Alias qui repellendus et ipsa a qui. Omnis saepe exercitationem animi similique debitis.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(13, 7, 7, 'Nam neque enim nisi voluptas dolor dolorum. Sequi nisi neque similique eos. Ab eius aperiam praesentium quia accusantium sit. Cupiditate aperiam et eum. Et et quos autem iure aut.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(14, 2, 5, 'Assumenda quis odio aut reprehenderit sit ad vero. Voluptatem quia aliquid voluptatibus nemo. Assumenda quam aut accusamus dolorem vero sit ducimus aperiam. Quibusdam odio quia ut incidunt.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(15, 7, 4, 'Aut soluta sit quibusdam. Iure dolorem ut vero id. Dignissimos inventore esse dignissimos suscipit.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'impedit', 'Rerum eum maiores ut iusto quia. Sunt corrupti quam aliquam sit. Porro id qui voluptas.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(2, 'maxime', 'Praesentium ea qui nam odit omnis non. Consequatur ut corrupti quia tenetur odio. Voluptas aut eum aperiam reiciendis quisquam amet.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(3, 'excepturi', 'Unde quam maiores odit. Aut sed vel perspiciatis eaque. Quo minus id placeat sed deserunt consectetur. Et ut repudiandae ullam veniam porro et.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(4, 'saepe', 'Tempora sint quo fugiat minima deserunt eum in nobis. Quidem labore excepturi dicta fugit. Incidunt esse minus velit inventore eligendi ut eum. Sapiente sunt labore laudantium similique optio voluptate sequi exercitationem.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(5, 'dolor', 'Fugit enim suscipit totam necessitatibus laboriosam voluptate voluptatem. Doloribus et laudantium nam occaecati consequatur nesciunt. Sapiente possimus sint repellat enim itaque. Est voluptatem quasi temporibus et dolorum incidunt. Ut optio nihil dolore ea saepe.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(6, 'totam', 'Doloribus et illum id animi dignissimos. Ratione doloribus eveniet perspiciatis. Expedita et non inventore rerum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(7, 'numquam', 'Accusamus laudantium sit qui suscipit in sit nisi. Est asperiores sint quidem nulla dolor nostrum et. Quas maiores adipisci quod.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(8, 'ut', 'Itaque voluptatibus et accusamus corrupti voluptas. Esse voluptas aut suscipit nemo mollitia enim sit. Deleniti impedit ducimus eum ipsam perspiciatis distinctio.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(9, 'qui', 'Velit dignissimos reiciendis omnis nobis omnis. Quia officia reiciendis ab eaque totam vel expedita culpa. Molestiae omnis perferendis inventore repellat. Et impedit assumenda ut omnis architecto.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(10, 'tempore', 'Sequi sed corrupti non rem est sit. Iusto et repellat est vel. Tenetur magnam consequatur qui. Ex vitae sapiente animi enim corporis provident laborum. Numquam excepturi vero sit consectetur facere recusandae.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categorizables`
--

CREATE TABLE IF NOT EXISTS `categorizables` (
  `category_id` int(11) NOT NULL,
  `categorizable_id` int(11) NOT NULL,
  `categorizable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categorizables`
--

INSERT INTO `categorizables` (`category_id`, `categorizable_id`, `categorizable_type`) VALUES
(1, 1, 'App\\Models\\Product'),
(1, 2, 'App\\Models\\Product'),
(1, 3, 'App\\Models\\Product'),
(2, 4, 'App\\Models\\Product'),
(2, 5, 'App\\Models\\Product'),
(2, 6, 'App\\Models\\Product');

-- --------------------------------------------------------

--
-- Table structure for table `imageables`
--

CREATE TABLE IF NOT EXISTS `imageables` (
  `image_id` int(11) NOT NULL,
  `imageable_id` int(11) NOT NULL,
  `imageable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `imageables`
--

INSERT INTO `imageables` (`image_id`, `imageable_id`, `imageable_type`) VALUES
(1, 1, 'App\\Models\\Product'),
(2, 2, 'App\\Models\\Product'),
(3, 3, 'App\\Models\\Product'),
(4, 4, 'App\\Models\\Product'),
(5, 5, 'App\\Models\\Product'),
(6, 6, 'App\\Models\\Product'),
(1, 2, 'App\\Models\\ProductDetail'),
(3, 7, 'App\\Models\\ProductDetail'),
(21, 1, 'App\\Models\\User');

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image_type_id` int(11) NOT NULL,
  `path` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`id`, `name`, `image_type_id`, `path`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'non', 5, 'https://lorempixel.com/640/480/?52794', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(2, 'dolor', 2, 'https://lorempixel.com/640/480/?28877', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(3, 'alias', 1, 'https://lorempixel.com/640/480/?34930', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(4, 'laboriosam', 2, 'https://lorempixel.com/640/480/?53176', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(5, 'vel', 5, 'https://lorempixel.com/640/480/?42812', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(6, 'vel', 7, 'https://lorempixel.com/640/480/?71984', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(7, 'voluptas', 9, 'https://lorempixel.com/640/480/?60143', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(8, 'aspernatur', 5, 'https://lorempixel.com/640/480/?67109', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(9, 'quia', 4, 'https://lorempixel.com/640/480/?25244', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(10, 'voluptatem', 8, 'https://lorempixel.com/640/480/?48234', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(11, 'corporis', 9, 'https://lorempixel.com/640/480/?19258', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(12, 'est', 6, 'https://lorempixel.com/640/480/?45421', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(13, 'quos', 4, 'https://lorempixel.com/640/480/?68038', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(14, 'eum', 1, 'https://lorempixel.com/640/480/?32406', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(15, 'aperiam', 7, 'https://lorempixel.com/640/480/?99201', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(16, 'exercitationem', 3, 'https://lorempixel.com/640/480/?12199', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(17, 'dolorum', 1, 'https://lorempixel.com/640/480/?35104', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(18, 'dolorem', 4, 'https://lorempixel.com/640/480/?84120', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(19, 'voluptatem', 6, 'https://lorempixel.com/640/480/?97054', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(20, 'iusto', 4, 'https://lorempixel.com/640/480/?14183', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(21, 'cat.jpg', 1, 'storage/img/user/VIFPqrLCpd0xebuyZuPM1UYCESzKQ8kSfEiRvE0z.jpeg', '2020-10-10 23:43:20', '2020-10-10 23:43:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `image_types`
--

CREATE TABLE IF NOT EXISTS `image_types` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `measure` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `image_types`
--

INSERT INTO `image_types` (`id`, `name`, `description`, `measure`, `created_at`, `updated_at`) VALUES
(1, 'ut', 'In ad maxime deleniti sunt natus iste nihil et. Facilis cum voluptas perferendis nam. Ipsam aut debitis molestias ex distinctio. Reiciendis nihil praesentium architecto ex et.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(2, 'quisquam', 'Voluptatibus aut voluptatem molestiae velit voluptatibus asperiores reprehenderit. Ipsa laboriosam molestias id est. Dignissimos est enim quasi nobis est totam. Qui soluta expedita nihil sit dolor.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(3, 'nihil', 'Quis non corporis harum vitae maiores. Voluptatibus amet accusantium earum sed. Vitae provident cum est possimus. Soluta vero quod ut explicabo.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(4, 'esse', 'Earum ut rerum facilis id. Doloremque repellendus aliquid cupiditate similique ducimus quae sit.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(5, 'repudiandae', 'Et velit cumque in ut quae ullam quidem. Fugiat in quis voluptatem. Laboriosam excepturi sed ut tempora eos occaecati corrupti. Magnam fugiat enim eaque consequatur asperiores qui molestias.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(6, 'temporibus', 'Aut placeat et sit aspernatur ut. Ad reiciendis mollitia sit optio at sed. Libero quisquam vel ipsum laboriosam quo.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(7, 'quae', 'Reprehenderit rem voluptates quidem voluptatem odio. Qui dolorum voluptas et repudiandae molestiae non provident. Est laborum veritatis sit incidunt quas similique ipsa. Maiores ut eum quia.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(8, 'sed', 'Ipsam ipsam eum ullam quia qui labore. Libero minus atque itaque aut. Quibusdam molestiae consequatur aspernatur vel saepe quae. Amet illum voluptatem non.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(9, 'et', 'Ea reprehenderit error amet qui natus magnam distinctio. Qui veritatis fugit eum harum est nulla blanditiis. Non dolorem est saepe. Molestiae dicta dolores consequatur laudantium commodi culpa fugiat.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(10, 'aperiam', 'Non ipsum illo eos autem nesciunt. Quia error assumenda perspiciatis ut neque provident perferendis quis. Et qui commodi sunt iusto odio voluptatem qui. Laborum ratione expedita aut quaerat et aliquid.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(11, 'impedit', 'Nesciunt et deleniti porro architecto ut quia. Fuga voluptas aut nisi. Sunt non veritatis ut quis autem.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(12, 'fuga', 'Laborum atque sunt velit unde et sit. Reprehenderit praesentium repellendus repudiandae exercitationem. Voluptatem nesciunt doloremque nostrum possimus laudantium ipsa quos. Non ut quisquam dolore molestiae ratione.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(13, 'quisquam', 'Sint non deleniti accusamus iure cum eveniet sit. Perferendis fuga ea accusantium ea dolore non. Enim assumenda ipsum et similique voluptatem rerum debitis. Et perspiciatis adipisci vel quas eos.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(14, 'magnam', 'Illum quidem similique est sint. Atque impedit iste assumenda rerum sint consectetur. Officia odio vel ipsa sunt aperiam nihil.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(15, 'provident', 'Placeat repudiandae omnis corrupti placeat. Officia rerum facilis sed illo nihil quae. Et magnam quia fugit tempora quam.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(16, 'nulla', 'Consequatur similique id doloremque occaecati aut voluptate. Repudiandae beatae dicta nihil voluptas. Voluptatem vero eveniet ut reprehenderit voluptatem blanditiis.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(17, 'exercitationem', 'Soluta consequuntur expedita nulla dolores eum. Est ut minima ullam quo voluptatibus sint eos.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(18, 'et', 'At atque voluptatum sunt vitae. Qui facere eveniet reiciendis.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(19, 'placeat', 'Et ea laborum atque esse. Architecto delectus quaerat cum dolorem vel in nisi. Porro commodi odio fugiat quia id.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(20, 'est', 'Et quisquam ut incidunt placeat veritatis deserunt ab iure. Molestias quos iure omnis iure doloribus quam. Dolorem minima explicabo non ab. Laudantium magni sunt deleniti ipsum dicta sunt temporibus.', '10;20', '2020-10-04 20:32:43', '2020-10-04 20:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `remark` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2020_09_04_114044_create_categories_table', 1),
(9, '2020_09_04_114803_create_categorizables_table', 1),
(10, '2020_09_04_114813_create_products_table', 1),
(11, '2020_09_04_114824_create_product_code_types_table', 1),
(12, '2020_09_04_114828_create_images_table', 1),
(13, '2020_09_04_114831_create_image_types_table', 1),
(14, '2020_09_04_114835_create_imageables_table', 1),
(15, '2020_09_04_114839_create_option_values_table', 1),
(16, '2020_09_04_114843_create_options_table', 1),
(17, '2020_09_04_114848_create_carts_table', 1),
(18, '2020_09_04_114851_create_cartables_table', 1),
(19, '2020_09_04_114857_create_orders_table', 1),
(20, '2020_09_04_114902_create_roles_table', 1),
(21, '2020_09_04_114907_create_user_roles_table', 1),
(22, '2020_09_04_114911_create_user_payments_table', 1),
(23, '2020_09_04_114915_create_payments_table', 1),
(24, '2020_09_04_114922_create_invoices_table', 1),
(25, '2020_09_04_120759_create_product_details_table', 1),
(26, '2020_09_26_120016_add_social_login_field', 1),
(27, '2020_09_27_103543_create_addcarts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_auth_codes_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'omUmjXfQdgTbP7sK56BYXG7ogiJLk4EoLWm76ljx', NULL, 'http://localhost', 1, 0, 0, '2020-10-04 20:32:52', '2020-10-04 20:32:52'),
(2, NULL, 'Laravel Password Grant Client', 'eX0DzxTuSUJacVj5ZDnqL6JjzohcwrUOCuCwiAu5', 'users', 'http://localhost', 0, 1, 0, '2020-10-04 20:32:52', '2020-10-04 20:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-10-04 20:32:52', '2020-10-04 20:32:52');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'amet', 'Nemo aut incidunt nesciunt vitae eum quis dolorem laudantium. Dolorum aut iusto veniam officiis. Ut eum aut cumque repudiandae dolor neque unde unde.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(2, 'optio', 'Aut est dolorem ullam minus et itaque. Qui tempora magni recusandae repudiandae. Non cum in tempore rerum ut.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(3, 'odio', 'Excepturi qui laboriosam possimus est ut. Magnam autem culpa rerum odit possimus quo debitis. Omnis esse officia cum dolores numquam expedita corrupti. Rerum optio optio dolor earum a libero accusantium.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(4, 'maxime', 'Quaerat dolor est at sunt consequatur doloremque. Suscipit labore aut reiciendis rerum veritatis at hic. Fugiat consequatur expedita est at. Autem nostrum et dignissimos quia. Aperiam molestiae repudiandae ut ea.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(5, 'dolorum', 'Pariatur quam sunt itaque aperiam velit magni dolores. Incidunt provident commodi omnis numquam non. Dolores soluta officia velit.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(6, 'dolor', 'Esse quibusdam hic est voluptate. Adipisci quidem quos eum magni ipsum et architecto. Et cupiditate in temporibus minus in accusamus. Accusamus ipsam excepturi reiciendis dignissimos itaque.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(7, 'saepe', 'Et in accusantium libero repellat sit inventore ut. Ex nostrum et sit veniam praesentium. Illum nulla voluptas quis dicta placeat nulla.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(8, 'dolore', 'Libero voluptatum non accusamus alias. Et expedita praesentium vel perferendis autem. Et sit consectetur qui similique ut occaecati sed. Vel id ipsam blanditiis et.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(9, 'suscipit', 'Perspiciatis deserunt et optio et ut et esse illum. Nihil quos perspiciatis fugiat molestiae eveniet. Dolor aut ratione distinctio praesentium pariatur culpa. Id illo quia qui qui incidunt consectetur saepe.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(10, 'perspiciatis', 'Ut exercitationem sint non nesciunt. Minima ut et voluptatem non. Rerum magnam ex earum et eaque qui.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(11, 'et', 'Et dolor doloribus nesciunt. Provident sunt illum voluptas aliquam dolorum. Dolor repudiandae dicta maxime quasi id.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(12, 'nam', 'Voluptate ut ducimus ad. Dolorem ut quod eum placeat dolores. Sed et ipsa illo dolor molestias et laborum. Error porro quia doloribus eum minus aut pariatur. Consectetur totam ad molestiae fugiat.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(13, 'enim', 'Doloremque omnis labore qui reprehenderit aut sint aut nesciunt. Ducimus laudantium illum ducimus voluptatibus odio omnis qui atque. Natus harum est voluptas earum ipsam. Provident ipsum natus voluptas ut saepe.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(14, 'consectetur', 'Laudantium tempora dicta quisquam qui provident error voluptatem. Quis voluptas dolor suscipit voluptatem in. Non cum sed odio totam dolores assumenda. Et ea expedita odio doloribus natus qui voluptatem. Sit blanditiis itaque eius molestiae dignissimos excepturi.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(15, 'eligendi', 'Temporibus cumque et velit fuga natus dicta atque. Consequatur repudiandae voluptatem molestiae. Assumenda aut et placeat labore accusantium ut.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(16, 'sed', 'Aut laudantium nobis repellat accusantium harum similique. At ad voluptate ipsa rem corrupti laborum ut. Rem id excepturi voluptate nihil laboriosam non earum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(17, 'doloribus', 'Debitis vero dolores officiis quis. Natus ut ea aut nam quae. Omnis temporibus est non repellat impedit est facere.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(18, 'explicabo', 'Deserunt voluptates magni fuga inventore ut exercitationem ipsam soluta. Qui quae voluptas quam et accusantium non commodi. Ut totam ab alias odit debitis. Enim accusantium quas pariatur.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(19, 'aut', 'Pariatur odit eligendi sit qui. Quae inventore nostrum aperiam doloribus fugiat. Consequuntur omnis itaque tempore expedita ut quam. Est dolorem aut necessitatibus ipsa.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(20, 'quam', 'Qui excepturi sed alias itaque nulla quibusdam ut. Corrupti vel fugit ab consequatur optio. Quia quia consequatur quidem culpa. Aliquam soluta porro amet sequi. Molestiae deserunt distinctio dolore aut.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(21, 'tempora', 'Qui harum et alias omnis ut veritatis. Voluptates eius enim neque ipsa voluptas excepturi quasi quia. Voluptatem aut magni pariatur eligendi culpa.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(22, 'incidunt', 'Doloremque necessitatibus veritatis maxime fugit et. Et sit placeat adipisci velit. Possimus aspernatur non minima omnis qui corrupti. Dolor hic ipsam sunt sint suscipit sit odio. Recusandae dolores iure aspernatur eveniet porro voluptate dignissimos.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(23, 'vel', 'Tempora sed et incidunt eum quia. Facere ut asperiores modi culpa. Aliquam repellendus doloremque id qui excepturi similique voluptates.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(24, 'animi', 'Qui aliquid sed ratione eos. Aut impedit ea placeat voluptatum in aut. Consequatur aperiam aut commodi placeat omnis. Velit voluptas modi rem qui omnis necessitatibus est praesentium. Autem qui quae quod aut dolores.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(25, 'odit', 'Porro dolorum et ea enim. Eveniet omnis repudiandae voluptatem. Amet nulla quos explicabo id aliquam aut.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(26, 'distinctio', 'Sunt voluptates recusandae dolores odio repellendus. Minus et quia est in qui. Nostrum explicabo odio beatae aut. Ut consequatur sunt rerum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(27, 'ut', 'Non sequi et facilis enim. In quasi omnis repellendus voluptatibus animi minus voluptate illo.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(28, 'omnis', 'Sed totam ea numquam cupiditate commodi. Repellat assumenda sunt perferendis omnis. Nihil blanditiis quia debitis sed asperiores.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(29, 'cupiditate', 'Et dolorem quasi dolor possimus cum nisi explicabo. Illum qui et occaecati dolores.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(30, 'iste', 'Et ut quia quae. Dolorem ab accusamus numquam sed. Omnis soluta quos quae aliquid. Sit saepe ut harum perspiciatis architecto esse.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(31, 'odit', 'Autem repudiandae quo ex dicta sint molestias. In reiciendis necessitatibus repellat debitis aspernatur. Laborum aut quasi voluptas. Mollitia id aut nisi enim id.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(32, 'possimus', 'Eligendi dolores omnis est voluptatem. At et assumenda aut reprehenderit omnis eos dolor sapiente. Aut amet cupiditate sequi.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(33, 'explicabo', 'Est sapiente ut sit voluptas aut officia qui. Et modi officiis ullam doloribus. Dolorum dolorum hic corrupti autem. Numquam quidem quis eligendi ut.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(34, 'autem', 'Molestias consequatur quos sint labore dolor. Pariatur rerum quo sint non quo vel illo. Itaque neque consequatur eos explicabo molestias minima ullam rerum. Necessitatibus iste illo deleniti dicta.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(35, 'consequatur', 'Pariatur aliquid quas ea quisquam accusamus quia perspiciatis. Velit delectus quis consectetur officiis officia assumenda non. Corporis mollitia minus non voluptas aut.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(36, 'libero', 'Consequatur et quasi ipsa ut unde. Ex qui odit optio. Explicabo aut nesciunt ipsum illo architecto asperiores velit culpa. Assumenda at maiores sint.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(37, 'voluptatem', 'Esse aut dolore et et. Voluptatum hic omnis enim sit. Esse iure dicta aut iusto nobis ea et consectetur. Totam quasi magnam quia quo qui. Aperiam facere molestiae hic dignissimos.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(38, 'dicta', 'Dolores excepturi deleniti velit officiis impedit libero atque. Ab exercitationem qui quibusdam quis. Est omnis vero cumque maiores ullam necessitatibus voluptas. Consequatur ut velit sed sit rerum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(39, 'asperiores', 'Facilis architecto alias accusantium maxime ducimus ea dolorem similique. Tempore quia temporibus a voluptas neque soluta alias. Et ut dolor et minus molestiae natus suscipit et. Perspiciatis magnam possimus aut.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(40, 'in', 'Molestiae ut incidunt temporibus enim. Corrupti fugit minima amet voluptatem. Veritatis nobis sapiente molestiae. Et tempora nobis quisquam.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(41, 'cumque', 'Veniam et libero quis nisi sit odit suscipit. Sed id optio quidem tempore ea modi consequuntur. Ut sit iusto libero unde corrupti optio asperiores.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(42, 'aut', 'Sit qui aspernatur non ducimus eveniet quam. At non rerum quo nobis. Eaque culpa blanditiis est ea qui non beatae impedit. Soluta itaque quo distinctio ut qui rerum a.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(43, 'ut', 'Vero facilis labore quia ducimus aliquid porro sed. Aperiam eveniet et veniam fugit et natus sint. Mollitia inventore animi dolor voluptatem dicta voluptate impedit dolores.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(44, 'laudantium', 'Commodi provident soluta omnis ex non id. At repellat sed qui et cupiditate dolorem. Vero consectetur voluptas beatae dolorem. Assumenda at nulla possimus tempora dolor. Consequatur modi suscipit non cupiditate tempore facere tempora.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(45, 'fugit', 'Facere ipsam nulla repellat qui neque. Molestiae qui vitae quisquam nihil enim ipsa assumenda. Commodi velit quisquam ea id. Et sequi dolor nisi quaerat exercitationem quidem.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(46, 'voluptatem', 'Voluptas culpa dolor in a dolores error. Fugiat harum minima atque atque sed recusandae molestiae. Quaerat aspernatur minus ea iusto maxime libero ut eos.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(47, 'impedit', 'In et eveniet vitae ex dolor molestiae. Eius hic quisquam cumque fuga quas perferendis consequatur suscipit. Eaque consequatur iusto exercitationem quia eos sint. Sapiente sint similique ut ipsum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(48, 'eos', 'Laborum officia laborum sint rerum iste eveniet rerum. Ad aliquam ratione aut repellat. Quia tempora ea saepe totam autem et ducimus. Ex et dolores voluptatibus quod.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(49, 'quis', 'Numquam natus exercitationem omnis dolorem iste autem consequatur. Non neque ut doloribus magnam. Saepe facilis pariatur cumque iste dolore vero fugiat et.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(50, 'quis', 'Facere commodi et voluptas quia voluptates. Eveniet qui sint at doloribus aut. Quia magnam nemo iusto voluptas.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `option_values`
--

CREATE TABLE IF NOT EXISTS `option_values` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `option_values`
--

INSERT INTO `option_values` (`id`, `option_id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'excepturi', 'Impedit voluptatem nesciunt similique dolor magnam laboriosam et. Similique ut est incidunt laboriosam quisquam repellat. Esse nesciunt assumenda repudiandae quia voluptas quae voluptatem corporis.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(2, 3, 'illo', 'Consequatur aliquid labore aut asperiores saepe. Omnis minima error ut vel eos quas omnis. Saepe in non dicta quo rerum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(3, 3, 'reprehenderit', 'Accusantium facere aliquid sapiente nulla ducimus fugiat non. Blanditiis et maiores ut molestias. Dolor at et autem rerum rerum sint.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(4, 7, 'saepe', 'Et magnam ipsam culpa incidunt sit perspiciatis. Quia qui cum sed neque. Inventore consequuntur sint provident sit autem.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(5, 3, 'odio', 'Atque enim modi qui tempore eaque quasi. Inventore qui rem sed ea fugit quasi adipisci. Praesentium culpa consequuntur sequi.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(6, 4, 'rerum', 'Repellat voluptatem voluptatum neque expedita. Amet eius atque repudiandae in.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(7, 7, 'aperiam', 'Et rerum quisquam ipsa sint esse est veritatis id. Provident reprehenderit qui voluptatem repellat impedit voluptatibus id. Deleniti explicabo omnis dolor tempora exercitationem pariatur. Impedit a dignissimos doloremque odit veritatis facere odit.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(8, 9, 'omnis', 'Aliquam voluptatem nam nisi maiores et necessitatibus. Qui amet magni veniam officia magnam quia accusantium. Voluptate et numquam ut culpa maxime. Aut necessitatibus quo non et.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(9, 3, 'blanditiis', 'Commodi repellendus molestias nam quos corrupti. Blanditiis doloremque soluta omnis ut laboriosam ea consequatur tenetur. Quaerat atque vero recusandae. Est totam eveniet sapiente aut.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(10, 7, 'reprehenderit', 'Quasi dolorem eum esse rerum pariatur maiores accusantium. Molestiae veniam quos rerum et culpa consequatur cumque quaerat. Fugiat perferendis facere aspernatur repellendus voluptatem.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(11, 8, 'eos', 'Nemo sequi ut amet nam. Enim dolor facilis non soluta. Assumenda hic consectetur et sit accusamus illum impedit.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(12, 4, 'explicabo', 'Perspiciatis qui aut amet odit rerum dolores non. Ullam est nulla in sit. Est occaecati vel cum. Et praesentium eum labore possimus dolorem.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(13, 2, 'distinctio', 'Quia iste sit iste dolorem eius iusto blanditiis quod. Natus qui vel eius est molestiae. Consequatur expedita quam eos minima veniam pariatur.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(14, 8, 'quo', 'Ut voluptas ducimus earum quod eum error. Eligendi cumque reprehenderit quia placeat nam corporis veniam. Enim perferendis quo voluptatem doloremque aspernatur iure.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(15, 6, 'molestias', 'Qui dolor molestias quidem animi numquam voluptatem et. Non eos blanditiis in natus fugit repellat eveniet. Minima cupiditate qui ut amet. Quam nam eligendi sapiente et in tenetur.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(16, 2, 'mollitia', 'Et ipsum corporis error eos. Totam enim laborum ad libero. Fugit ea eligendi autem qui blanditiis accusamus doloribus.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(17, 1, 'qui', 'Unde enim ut voluptates repellendus est qui. Aut aut aperiam dolor excepturi aliquid laborum doloribus. Fugiat odit consequuntur nesciunt ad. Omnis inventore maiores aut nihil. Quasi dolor ut aut animi iste nesciunt.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(18, 2, 'vel', 'A ut impedit optio sed illo eaque. Suscipit ea aliquid quibusdam qui animi. Ducimus ut ipsum eos qui.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(19, 4, 'molestiae', 'Quibusdam laudantium dolores autem aspernatur. Eum laborum molestiae officia eum aspernatur. Molestias qui voluptas repellat vero earum dolor laudantium.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(20, 3, 'rem', 'Et aspernatur odio laudantium et. Optio consequatur veritatis debitis quisquam porro qui. Hic illo possimus consequatur dolorum vel.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(21, 6, 'sit', 'Aliquam sapiente eligendi iste quaerat dolore. Odit sequi labore similique maxime. Laborum est ex quia modi est accusamus. Velit eveniet non odit facere.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(22, 3, 'sint', 'Et praesentium voluptatem iure dolores ut reprehenderit adipisci. Impedit dicta et dolores doloremque tempora delectus ipsa. Eos vero repellendus sit placeat voluptas consequuntur. Labore et nesciunt itaque recusandae dignissimos odio.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(23, 5, 'blanditiis', 'Minima eaque quasi expedita vitae facilis consequatur. Perferendis et fugiat sapiente consequuntur asperiores odit. Magnam at ipsum iste veritatis ipsam ea.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(24, 6, 'est', 'Quidem minus ex sed et dolore quo eligendi similique. Eligendi dolore quam est velit cumque unde libero. Error mollitia commodi accusantium.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(25, 6, 'dolores', 'Ad molestias sed autem. Soluta quia non architecto nulla sapiente est. Ex excepturi sunt aliquid et cumque accusamus et. Ducimus perferendis placeat quidem rem doloremque aut.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(26, 2, 'molestias', 'Laborum at consequatur adipisci ducimus exercitationem eum deleniti illo. Rerum quidem ea officiis molestiae. Porro corrupti aliquam saepe vitae.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(27, 3, 'ex', 'Sequi doloribus fuga tempore at dolor praesentium. Inventore rerum illo tempora.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(28, 1, 'voluptas', 'Nostrum illum omnis itaque accusantium similique illo. Aut facere delectus quos occaecati expedita culpa. Qui voluptatem aut dolor voluptas veritatis cum velit omnis.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(29, 4, 'hic', 'Cupiditate esse id voluptatem dolores animi voluptas aut nihil. Voluptas consequatur beatae a adipisci. Numquam aut quaerat quod commodi voluptates eos libero esse. Blanditiis architecto veniam ut corporis nisi numquam.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(30, 1, 'et', 'Officiis nemo sed aut nesciunt id. Ea nemo et modi quis asperiores. Qui voluptatem non qui temporibus illo et. Libero magnam vitae earum perspiciatis aut qui quia.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(31, 2, 'aspernatur', 'Aut quaerat ducimus impedit voluptatem ipsa. Commodi voluptas dolor voluptatem ea. Architecto eum accusantium consequatur cupiditate. Qui tempora non et laboriosam qui temporibus praesentium.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(32, 5, 'enim', 'Velit et quo dignissimos nobis. Consequatur voluptatem harum perspiciatis quia. Corrupti odit perspiciatis rerum aperiam.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(33, 4, 'nostrum', 'Esse delectus nesciunt quos consequatur sapiente magni harum. Nihil sint dolores accusantium optio et officia. Itaque omnis unde ipsa voluptate aliquid voluptas voluptatem.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(34, 9, 'consequatur', 'Voluptatem dolores nisi omnis dolores. Quisquam molestias aut ea voluptatem odit. Est eum aut totam voluptas dolore et molestias. Iure recusandae sunt sunt aperiam.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(35, 2, 'ut', 'Earum nobis explicabo eos quia quia sapiente. Mollitia esse quo sunt sed aut nostrum laborum aut. Ipsam commodi nisi ea ab consectetur.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(36, 6, 'doloribus', 'Aspernatur quisquam hic repellat et similique. Sequi sint praesentium porro aut autem reiciendis. Voluptatem iusto omnis et consequuntur rem aut quo sed. Voluptatem est a ut quia qui magnam.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(37, 1, 'ut', 'Recusandae voluptatem vel repudiandae quam ut ut exercitationem. Iusto provident voluptatem quam animi sit sapiente ut. Eligendi necessitatibus et rerum autem illum voluptate.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(38, 6, 'quis', 'Ad sapiente et quia possimus facere consequuntur. Sit aut laborum tempore sequi dolorum minima doloremque. Quod perspiciatis at eaque aut aut iste. Eligendi repellat corporis et dolor rerum vel animi.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(39, 2, 'sapiente', 'Sit beatae eligendi ut repellat expedita labore consequatur. Repudiandae et maiores nostrum commodi exercitationem quis animi. Commodi velit rerum libero rerum repellendus necessitatibus. Corrupti maiores atque esse est.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(40, 8, 'odit', 'Qui facere cum aut aut ipsum delectus ut dignissimos. Et facere officia placeat. Odio sunt fugiat totam voluptate corporis est iste. Ipsum omnis eaque sed et nobis non rem.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(41, 9, 'doloremque', 'Qui in et aperiam velit. Voluptatem nostrum soluta eos fuga ea. Molestias dolores sit maiores eveniet.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(42, 7, 'odit', 'Quo velit incidunt magnam quibusdam. Sint est reprehenderit odit blanditiis qui ipsa. Vero unde enim impedit facilis ea.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(43, 5, 'vitae', 'Temporibus neque est nesciunt minus modi porro. Soluta et est sed error iste eos.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(44, 5, 'deleniti', 'Quos in ipsam ipsum. Eveniet sunt aut facilis aliquam et dolorem qui.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(45, 6, 'et', 'Rerum vitae qui voluptatem. Illum accusantium consequatur at aliquam. Iusto quia ut corrupti ut eos amet quod.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(46, 2, 'sit', 'Iusto delectus quia et aut sit et aut facilis. Dolorem et est impedit expedita reiciendis et minima. Laborum fuga temporibus esse aut magnam. Quidem quam tenetur laborum omnis.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(47, 4, 'tempore', 'Hic minus unde temporibus et. Ea illo vel facilis neque quas. Similique excepturi sit et ut blanditiis ad ipsum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(48, 6, 'mollitia', 'Nihil amet aut mollitia sed eum laboriosam voluptates. Nostrum perferendis iusto esse ut nihil aut optio. Quibusdam quo aut sint sit. Id excepturi perspiciatis libero voluptatem.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(49, 2, 'atque', 'Sunt neque maiores illum consequatur ipsa accusamus. Qui natus velit earum optio. Recusandae est ipsam expedita neque. Nesciunt expedita provident numquam et. Facere itaque similique et tempora perferendis.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(50, 5, 'voluptatum', 'Consequatur rerum qui ipsum nam in. Sed magni ut impedit inventore in.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_date` date NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `status_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_date`, `quantity`, `description`, `status_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '2003-10-29', 9, 'Iusto aut aut aperiam saepe. Eius minus natus aperiam et veritatis eos amet.', 7, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(2, '2019-05-18', 4, 'Velit repudiandae fugit ad rerum corporis doloribus omnis recusandae. Non nisi excepturi enim velit at ad. Beatae ut deserunt perspiciatis voluptatem.', 5, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(3, '1993-11-24', 5, 'Quia enim officiis fugiat aliquam temporibus. Molestiae rerum esse aliquam sint. Incidunt dolor fugit voluptatem ut molestias provident ducimus.', 8, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(4, '1991-05-12', 5, 'Officia tempore eius blanditiis ut in. Rem minima qui necessitatibus praesentium dolorem delectus ut assumenda. Fugiat quo quo atque voluptatem voluptatum.', 1, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(5, '1977-02-13', 1, 'Placeat quam explicabo explicabo consequatur laboriosam impedit. Beatae eos aut modi voluptatem culpa odit. Consectetur facilis officia harum sint impedit nemo totam. Aut explicabo ratione cum voluptatibus delectus deleniti.', 7, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(6, '2008-05-28', 4, 'Sed et deleniti culpa excepturi qui beatae. Unde et est dolor.', 4, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(7, '2009-03-29', 3, 'Ut voluptatem quis libero non unde aut. Ipsa ipsum aliquid eaque voluptatem. Enim in ea itaque non. Eaque libero accusamus velit.', 1, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(8, '2008-09-19', 2, 'Dolore quod pariatur ut. In in iusto enim est fuga numquam voluptates. Placeat eum odio voluptatem voluptate officia.', 7, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(9, '1981-03-10', 5, 'Inventore dolorem eos blanditiis dolor et. Provident itaque atque quidem officiis. Sint eius accusamus omnis veniam magni fugit odit. Possimus facilis itaque consectetur sequi impedit sint deleniti.', 2, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(10, '1976-07-28', 5, 'Sed sed suscipit aut reprehenderit perspiciatis illum. Quae accusantium perspiciatis labore. Vero et sed voluptas dolores hic error.', 9, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(11, '1992-02-01', 9, 'Tempore ut eos tenetur ipsum et. Autem voluptatum deserunt magni vel magnam et.', 5, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(12, '1973-05-21', 7, 'Qui iste laboriosam cupiditate pariatur enim. Doloribus consequatur alias amet odio sed sapiente voluptatibus. Nisi quasi aut nisi qui aspernatur et. Veritatis et saepe ratione in.', 7, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(13, '2006-03-02', 5, 'Hic vero unde repellendus architecto nemo. Voluptatem velit aut nihil aut. Facilis placeat eos maiores voluptatibus soluta. Sint temporibus est in voluptatem dolores.', 1, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(14, '1986-09-12', 4, 'Sed quidem quisquam dolores. Facere sequi voluptates repellendus ducimus. Ut asperiores facilis sit magni officia et mollitia.', 1, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(15, '1982-12-11', 1, 'Sed magni deleniti vitae dolorem voluptatum id quaerat. Provident ullam non amet pariatur rerum. Dignissimos doloribus sunt non laboriosam molestias aut earum. Id animi sint odio eligendi adipisci.', 5, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(16, '2011-10-29', 9, 'Fuga voluptatibus aliquid explicabo molestias. Aut maiores perferendis corrupti. Id officia asperiores qui et similique voluptas excepturi. Soluta quis omnis quis ut.', 7, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(17, '2012-02-11', 2, 'Aut dolores dicta quia aliquam rem. Expedita facilis eum quae illo. Id quia ab qui voluptatum illo nulla.', 7, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(18, '1995-06-03', 2, 'Exercitationem quasi sed sit quis nesciunt quo similique. Nihil fuga vero in maiores.', 8, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(19, '1972-06-26', 2, 'Voluptatem culpa laudantium rem veniam voluptas quos. Ducimus ut rerum molestiae asperiores hic. Assumenda nobis excepturi consequuntur amet et placeat et. Est aperiam cupiditate hic voluptas soluta porro. Et error distinctio ea ut est sit.', 1, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(20, '1985-04-03', 4, 'Neque rerum quia omnis voluptatem neque. Occaecati enim voluptates rerum qui et qui eius. Velit consequuntur atque nihil veniam. Et et reprehenderit et et maiores.', 4, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE IF NOT EXISTS `payments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `user_payment_id` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `amount` double(8,2) NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code_type_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `product_code`, `product_code_type_id`, `name`, `description`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '422434698', 5, 'consequatur', 'Aut odit ut illum doloribus non. Cupiditate non officiis fugit inventore. Quas dolores earum iure delectus.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(2, '75', 27609789, 'quas', 'Illo qui enim porro optio ut qui. Nisi neque aliquam doloremque ratione ut doloribus earum. Laborum fugit laborum accusantium perspiciatis quae nulla qui.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(3, '216', 717817, 'consequuntur', 'Et libero incidunt facilis eius. Assumenda provident pariatur et cum et perspiciatis. Dolorem a quibusdam labore rerum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(4, '616846619', 15939, 'quas', 'Labore provident unde eum quasi eum. Quam quod qui ipsam voluptatem quisquam corrupti quidem. Quia laudantium quam perspiciatis eligendi eum. Iure iure aut libero et id.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(5, '36809991', 807022, 'aut', 'Quia omnis tempora eveniet voluptatum repudiandae culpa quasi. Ut facilis delectus deleniti eligendi rem ut quae non. Fugit illum ex natus velit ab.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(6, '4146', 941293997, 'in', 'Omnis enim minus nihil libero quia exercitationem. Voluptatem quae quidem sint commodi ut. Sint rerum expedita quo velit fuga.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(7, '513', 92739, 'rerum', 'Nesciunt et rerum et qui ut non. Amet corrupti dicta quas. Iure aut unde quis quia nesciunt nam tenetur. Et aut et velit voluptatem ea est et.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(8, '557', 673639, 'velit', 'Unde natus est accusamus vel. Voluptatibus molestiae odio et accusamus maxime eos. Ipsam sit nostrum mollitia officia ullam non.', '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(9, '992348478', 4381, 'nisi', 'Voluptatem eum reprehenderit voluptates voluptas iste. Et nobis rerum necessitatibus et nam eos repellendus. Dolores saepe consequatur qui quo. Eum earum sed tempore consequatur distinctio repudiandae quas.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(10, '28403881', 11001107, 'labore', 'Consequatur ratione est odit expedita ea voluptatem qui. Qui in voluptatem optio porro debitis incidunt aut incidunt. Quas laudantium veniam inventore veritatis et magnam quisquam nobis. Rem amet voluptatum aspernatur sapiente nostrum quas.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(11, '1', 14981, 'non', 'Et ullam corporis aspernatur rem. Eligendi voluptatem vel quasi consectetur voluptatem sint. Ex est ut vero facere itaque ipsa. Voluptatem rerum est recusandae qui.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(12, '5810333', 7649, 'quisquam', 'Commodi fuga ipsum vel repudiandae nisi impedit aliquid. Nesciunt non laudantium non ullam officiis explicabo eaque.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(13, '23', 719382, 'non', 'Non sequi porro quo quo exercitationem. Et fugiat optio repellendus dolor ea. Molestiae illum at doloribus quia sit. Molestiae molestiae ea hic asperiores expedita maxime quibusdam adipisci.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(14, '21', 939, 'ut', 'Eum accusamus voluptatem et consectetur. Nostrum sed vitae non voluptas sequi. Modi nemo sunt sit eum eum itaque.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(15, '349', 61992043, 'velit', 'Labore non quod autem assumenda dolores doloribus illo. Provident in nisi dolore nobis eum debitis. Pariatur ea dicta ab et quis eveniet.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(16, '3643', 246427, 'consequatur', 'Sunt qui dolorem consequuntur deserunt quos. Amet ad tempora amet perspiciatis aut ut.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(17, '485893041', 0, 'id', 'Earum necessitatibus aperiam sit ipsa expedita esse repudiandae. Dolorem quis quibusdam consectetur dicta sunt. Ut modi aperiam rerum ratione voluptatem repellat in.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(18, '743745', 11, 'et', 'Alias aut nobis totam magnam distinctio. Distinctio doloremque repellendus qui maiores esse nostrum quo aliquam. Doloribus eos vitae quisquam architecto quo quia.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(19, '6393218', 102258577, 'est', 'Debitis et quam quo facilis cumque perferendis esse. Dolor officiis minus necessitatibus qui consectetur rerum dolore. Nobis alias iure atque non.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(20, '70494', 23425, 'neque', 'Dolores reprehenderit ad ipsam quasi ad pariatur. Quidem est et quo quia. Non omnis sed fugit vel iusto.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(21, '8092', 705329, 'consequatur', 'Corrupti tempore qui eligendi laborum non. Et aperiam occaecati consequatur nemo fugiat cumque. Esse natus ducimus voluptate.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(22, '7996', 2889033, 'et', 'Dolor laborum quod perferendis voluptatibus ab amet quia. Blanditiis nulla quas vitae amet rem provident qui pariatur. Tempore similique eos expedita.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(23, '41916438', 496410, 'unde', 'Vel sed autem esse possimus qui sunt. Voluptatibus asperiores autem placeat itaque hic repudiandae. Ducimus harum quis velit dolorem commodi. Quo aut et sed beatae et qui blanditiis doloremque.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(24, '37', 52, 'molestiae', 'Eum molestiae accusamus optio dolor iste. Odit accusamus architecto omnis provident beatae harum. Omnis velit eveniet accusantium voluptatibus ut quam rem.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(25, '822766', 3, 'consequatur', 'Est et et et debitis rerum temporibus. Officia enim reiciendis mollitia qui ut. Nihil veritatis et voluptatem nisi provident autem aut.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(26, '2554', 2, 'quae', 'Et ut quis exercitationem voluptatibus architecto. Adipisci velit tempore dolorem qui. Quisquam magni molestiae laborum ad. Provident impedit saepe quis omnis sed. Sit recusandae qui quia natus culpa quis quos.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(27, '66679', 6632, 'quia', 'Maiores accusantium vitae suscipit excepturi tempore beatae possimus. In nesciunt error totam repudiandae explicabo accusantium architecto. Sed velit sunt perspiciatis.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(28, '739', 2784710, 'occaecati', 'Ea natus est ducimus repudiandae. Earum facilis et ipsum eos vitae occaecati dolorem vero. Et accusamus eos dolore ut. Reprehenderit molestiae et ea doloribus earum.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(29, '2', 2, 'quia', 'A sapiente dolor ipsum deserunt laudantium consequatur dicta. Magni optio exercitationem consequatur doloribus nesciunt natus. Qui ipsa tempore nemo. Nostrum aut adipisci neque.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(30, '1', 41, 'non', 'Deserunt quod nisi quo atque laborum accusantium fugit. Aliquam alias qui a et. Et eveniet natus harum sit cum rerum voluptatem vel. Et id laborum aut sint.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(31, '812', 43931, 'et', 'Voluptatem voluptatem tempore fuga iusto non voluptatum. Quia ab veritatis nisi aut recusandae. Cum necessitatibus esse adipisci.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(32, '8172', 32, 'qui', 'Nam nesciunt ratione veniam ducimus et laudantium. Suscipit velit exercitationem quas sint qui. Optio non adipisci autem corporis. Tempora molestias necessitatibus ipsa autem voluptatem.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(33, '4', 786783, 'aliquam', 'Consequuntur culpa non nihil velit. Itaque enim cumque et minima. Perspiciatis quas dignissimos repellat beatae eum voluptatem harum. Minima facere id voluptatem consequatur quos debitis.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(34, '54097537', 6761581, 'officiis', 'Porro impedit et et maxime quia. Ex iusto veritatis sint cumque voluptate delectus. Doloremque qui ut itaque consequuntur expedita ut. Voluptatum sunt quod quo dolorem doloribus rerum minus.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(35, '263188950', 9, 'id', 'Adipisci temporibus cupiditate cumque alias facere aut. Et quas fuga qui enim doloribus. Dolorum tenetur ullam voluptas. Placeat consequatur alias aut.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(36, '4307156', 1967, 'explicabo', 'Omnis qui et sit facilis doloremque deleniti. Numquam officiis voluptatem et error incidunt cum. Doloribus atque dolor nihil.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(37, '3349', 12669029, 'in', 'Aperiam ducimus unde libero deserunt harum voluptatem cum. Natus totam alias eos corrupti fugiat. Et consectetur natus similique nesciunt dolorem et.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(38, '85233', 939517068, 'non', 'Maiores itaque ducimus voluptate ex consequuntur. Quas sed porro et quam assumenda. Et explicabo temporibus est nostrum. Vel in inventore praesentium provident odit quisquam minima ipsam.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(39, '78', 862679874, 'eius', 'Et aut labore asperiores eum corporis eos. Ut beatae quos ad quibusdam minima voluptatem deleniti. Nulla porro voluptatum maxime voluptates. Qui et dolor aut sequi repellat perspiciatis laudantium.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL),
(40, '3', 97917, 'reprehenderit', 'Autem itaque labore animi repudiandae dolore. Tempore libero atque quas nam est aspernatur cumque. Suscipit ratione qui sed ut quod molestiae. Omnis quo exercitationem quia rerum.', '2020-10-04 20:47:40', '2020-10-04 20:47:40', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_code_types`
--

CREATE TABLE IF NOT EXISTS `product_code_types` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `code_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` mediumtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_code_types`
--

INSERT INTO `product_code_types` (`id`, `code_type`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Commodi nesciunt et possimus reprehenderit reiciendis architecto.', 'Necessitatibus doloremque corrupti minima sequi explicabo quasi ipsam. Magni et qui quo odio dolores voluptatem perspiciatis consequuntur. Sed quos ad repellendus nihil. Omnis error animi fuga sit dolorum ab.', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(2, 'Cupiditate sunt cupiditate delectus exercitationem.', 'Numquam corrupti est vel architecto laborum quae aut. Cumque aut accusantium qui cupiditate voluptatem. A officiis ipsum ut explicabo expedita veniam. Deserunt doloribus nemo impedit.', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(3, 'Repellat voluptatibus error et et et velit explicabo.', 'Quasi id molestias molestias iste ut quod. Nostrum nostrum doloremque eaque quia. Est sed dolor qui nostrum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(4, 'Perferendis voluptatem harum mollitia aut aut.', 'Ullam molestias velit at non quasi nihil ducimus eos. Sint necessitatibus adipisci deserunt quia ipsum. Culpa aspernatur qui assumenda fugit eaque velit ipsa cum.', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(5, 'Ipsam quibusdam deserunt perspiciatis delectus nam non.', 'Alias consequuntur qui sit dolorum aut. Omnis possimus omnis culpa dignissimos omnis neque et est.', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(6, 'Et voluptatibus maxime velit dolorum adipisci eos mollitia.', 'Et qui rerum possimus necessitatibus cumque velit perferendis. Quisquam sed et et alias aliquam voluptatem. Atque quia quibusdam dolor deserunt minus perspiciatis. Aut eos dolorem in molestiae.', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(7, 'Nobis sed cumque delectus odit quam et vel.', 'Molestiae porro hic numquam saepe. Vero repellendus omnis sit aut dolore maxime. Sit est esse eaque qui est adipisci.', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(8, 'Sit voluptas necessitatibus ut hic.', 'Quia et et rerum aliquid quo sint. Fugit alias quae vel libero quis id vel quia. Possimus omnis beatae id velit et.', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(9, 'Deserunt qui voluptatem dolorum nostrum sint accusantium.', 'Quia est iure accusamus maiores. Quaerat et dolorum commodi error.', '2020-10-04 20:32:43', '2020-10-04 20:32:43'),
(10, 'Tenetur officiis eos ipsum quia est incidunt.', 'Alias nam dicta sequi laudantium. Est porro ducimus sint qui voluptates. Repellendus blanditiis ex quaerat. Quos voluptatum assumenda accusantium.', '2020-10-04 20:32:43', '2020-10-04 20:32:43');

-- --------------------------------------------------------

--
-- Table structure for table `product_details`
--

CREATE TABLE IF NOT EXISTS `product_details` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_detail_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `option_value_id` int(11) NOT NULL,
  `price` double(5,2) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_details`
--

INSERT INTO `product_details` (`id`, `product_id`, `product_detail_code`, `parent_id`, `option_id`, `option_value_id`, `price`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 2, '23', 2, 1, 8, 33.55, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(2, 3, '70508455', 5, 3, 8, 42.27, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(3, 4, '609565', 2, 6, 4, 75.31, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(4, 6, '30816872', 4, 9, 4, 36.05, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(5, 1, '739', 3, 7, 4, 20.77, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(6, 2, '211142016', 4, 9, 9, 47.23, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(7, 4, '547081', 4, 1, 7, 71.91, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(8, 8, '39', 5, 7, 3, 71.41, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(9, 5, '8', 7, 5, 4, 30.52, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(10, 7, '2558', 1, 3, 8, 85.02, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(11, 4, '477943201', 9, 6, 9, 24.39, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(12, 1, '337706', 8, 2, 2, 48.57, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(13, 1, '3522758', 9, 5, 3, 23.70, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(14, 9, '5971', 9, 6, 4, 69.61, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(15, 7, '9564654', 9, 3, 9, 90.17, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(16, 2, '1236598', 9, 8, 2, 79.58, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(17, 8, '151003424', 8, 7, 6, 53.94, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(18, 8, '83', 2, 6, 9, 67.14, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(19, 8, '152988', 6, 2, 1, 11.06, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(20, 8, '620091', 9, 2, 9, 27.93, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(21, 5, '70426249', 8, 5, 2, 99.00, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(22, 5, '4737', 4, 8, 3, 62.08, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(23, 3, '9', 7, 2, 7, 93.40, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(24, 8, '807217', 3, 9, 2, 64.41, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(25, 9, '3349', 3, 9, 7, 17.24, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(26, 8, '973069241', 6, 7, 8, 91.44, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(27, 8, '83765296', 7, 7, 4, 62.08, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(28, 3, '47625734', 8, 1, 2, 10.16, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(29, 2, '67123881', 2, 5, 2, 44.03, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(30, 7, '88', 4, 7, 3, 34.58, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(31, 5, '26', 9, 7, 9, 26.84, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(32, 1, '20', 1, 3, 4, 3.42, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(33, 4, '741', 8, 9, 8, 95.28, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(34, 3, '902884682', 8, 7, 2, 91.54, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(35, 6, '522968614', 5, 3, 8, 33.54, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(36, 8, '42870828', 5, 1, 1, 11.57, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(37, 4, '8', 2, 1, 2, 63.86, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(38, 9, '544', 5, 3, 1, 26.84, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(39, 4, '10584', 6, 3, 1, 34.29, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL),
(40, 6, '8369', 3, 7, 2, 47.75, '2020-10-04 20:32:43', '2020-10-04 20:32:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `social_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `phone`, `remember_token`, `created_at`, `updated_at`, `social_id`, `social_type`) VALUES
(1, 'Sureachio', 'sovannareach.darom28@gmail.com', NULL, '$2y$10$AYFZWL9Zmgkv502WTZKAgebg6vNrrf.yBBftMmJGMypZblxu5a/.O', '069392246', NULL, '2020-10-04 04:48:25', '2020-10-10 23:36:37', '116641803608393082530', 'google');

-- --------------------------------------------------------

--
-- Table structure for table `user_payments`
--

CREATE TABLE IF NOT EXISTS `user_payments` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cardholdername` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `credit_card_number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `expiredate` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cvv` int(11) NOT NULL,
  `remark` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

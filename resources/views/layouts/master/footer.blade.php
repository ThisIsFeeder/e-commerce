<footer class="main-footer text-sm">
  <strong>Copyright &copy; 2020 <a href="#">BiTribie.info</a>.</strong>
  All rights reserved.
  <div class="float-right d-none d-sm-inline-block">
    <b>Version</b> 0.0.2
  </div>
</footer>
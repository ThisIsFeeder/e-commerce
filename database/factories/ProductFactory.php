<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'product_code'=>$faker->randomNumber,
        'product_code_type_id'=>$faker->randomNumber,
        'name'=>$faker->word,
        'description'=>$faker->paragraph,
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use App\Models\Image;

$factory->define(Image::class, function (Faker $faker) {
    return [
        'name'=>$faker->word,
        'image_type_id'=>$faker->randomDigitNot(0),
        'path'=>$faker->imageUrl,
    ];
});

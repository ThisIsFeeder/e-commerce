<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cart extends Model
{
    use SoftDeletes;
    protected $fillable = ['quantity', 'description', 'product_detail_id'];

    public function productDetail()
    {
        return $this->belongsTo('App\Models\ProductDetail');
    }

    public function users()
    {
        return $this->morphedByMany('App\Models\User', "cartable");
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Invoice extends Model
{
    use SoftDeletes;
    protected $fillable = ['date', 'remark', 'status_id', 'order_id'];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }
}

@extends('master')

@section('content')
<div class="site-section">
	<div class="container h-100">
		<div class="row justify-content-md-center h-100">
			<div class="card-wrapper col-md-5">
				<div class="card card-danger card-outline">
					<div class="card-body box-profile">
						@if ($errors->any())
						<div class="alert alert-danger">
							<strong>{{__('Warning!')}}</strong>{{__(' Please check your input code')}}<br><br>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif

						<form role="form" action="" method="post"
							enctype="multipart/form-data">
							{{method_field('PUT')}}
                            {{csrf_field()}}
                            
							<div class="text-center" id="edit">
								<label for="name">{{__('Reset Password')}}</label>
							</div>

							<ul class="list-group list-group-unbordered mb-3">
								<li class="list-group-item">
									<div class="form-group">
										<label for="email">{{__('Email')}}</label>
										<input type="email" class="form-control" name="email">
									</div>
								</li>
							</ul>

							<button type="submit" class="btn btn-primary btn-block"><b>{{__('SEND PASSWORD RESET LINK')}}</b></button>
							<button onclick="location.href='{{ route('profile.index') }}'" type="reset"
								class="btn btn-outline-primary btn-block"><b>{{__('Cancel')}}</b></button>
						</form>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
		</div>
	</div>
</div>

@endsection
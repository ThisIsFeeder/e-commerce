<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\OptionValue;
use Faker\Generator as Faker;

$factory->define(OptionValue::class, function (Faker $faker) {
    return [
        'option_id'=>$faker->randomDigitNot(0),
        'name'=>$faker->word,
        'description'=>$faker->paragraph,
    ];
});

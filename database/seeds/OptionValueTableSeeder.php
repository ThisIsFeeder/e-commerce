<?php

use Illuminate\Database\Seeder;
use App\Models\OptionValue;
class OptionValueTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(OptionValue::class, 50)->create();
    }
}

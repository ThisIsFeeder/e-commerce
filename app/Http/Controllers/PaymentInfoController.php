<?php

namespace App\Http\Controllers;
use App\Models\UserPayment;
use Illuminate\Http\Request;
use Auth;

class PaymentInfoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        // $userpayments = UserPayment::all();
        // return view('payment.index', compact('userpayments'));
        return view('payment.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        // return view('payment.index');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'cardholdername'=>'required',
            'credit_card_number'=>'required',
            'expiredate'=>'required',
            'cvv'=>'required',
            'remark'=>'required'
        ]);
        $user = Auth::user();
        $createpaymentinfo = UserPayment::create(['cardholdername'=>$request['cardholdername'], 
        'credit_card_number'=>$request['credit_card_number'], 
        'expiredate'=>$request['expiredate'], 
        'cvv'=>$request['cvv'], 
        'remark'=>$request['remark'], 
        'user_id' => $user->id]);
        $createpaymentinfo->save();
        return redirect('/')->with('success', 'Payment Added');

        //  
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserPayment  $userPayment
     * @return \Illuminate\Http\Response
     */
    public function show(UserPayment $userPayment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(UserPayment $userPayment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserPayment  $userPayment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserPayment $userPayment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserPayment  $userPayment
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserPayment $userPayment)
    {
        //
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'path', 'image_type_id'];

    public function productCodeType()
    {
        return $this->belongsTo('App\Models\ImageType');
    }

    public function imageType() {
        return $this->belongsTo('App\Models\ImageType');
    }
    
    public function products() {
        return $this->morphedByMany('App\Models\Product', "imageable");
    }
}

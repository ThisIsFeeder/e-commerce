<?php
   
namespace App\Http\Controllers;
   
use App\Http\Controllers\Controller;
use Socialite;
use Auth;
use Exception;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
   
class SocialController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectTo($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleCallback($provider)
    {
        try {
     
            $user = Socialite::driver($provider)->stateless()->user();

            $finduser = User::where('social_id', $user->id)->first();
      
            if($finduser) {
      
                Auth::login($finduser);
     
                return redirect('/home');
      
            }
            else {
                $newUser = User::create([
                    'name' => $user->name,
                    'email' => $user->email,
                    'social_id' => $user->id,
                    'social_type' => $provider,
                    'password' => Hash::make('my-google')
                ]);
     
                Auth::login($newUser);
      
                return redirect('/home');
            }
     
        }
        catch (Exception $e) {
            dd($e);
        }
    }
}
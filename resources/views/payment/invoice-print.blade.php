<div class="invoice p-3 mb-3">
    <!-- Table row -->
    <h2>PAYMENT SUMMARY</h2>
    <div class="row">
        <div class="col-6 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Qty</th>
                        <th>Product</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>Blue denim shirt</td>
                        <td>$17.99</td>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Red hoodie</td>
                        <td>$35.99</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-6">
            <p class="lead">Payment Methods:</p>
            <img src="../dist/img/credit/visa.png" alt="Visa">
            <img src="../dist/img/credit/mastercard.png" alt="Mastercard">
            <img src="../dist/img/credit/american-express.png" alt="American Express">
            <img src="../dist/img/credit/paypal2.png" alt="Paypal">
            <p></p>

            <div class="card card-primary">
                <!-- <div class="card-header">
           <h3 class="card-title">Quick Example</h3>
         </div> -->
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form">
                    <div class="card-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">CARD NUMBER</label>
                            <input type="number" class="form-control" id="" placeholder="1234 - 5678 - 6325 - 6584">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">CVC</label>
                            <input type="number" class="form-control" id="exampleInputPassword1" placeholder="123">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">CARD HOLDER NAME</label>
                            <input type="number" class="form-control" id="exampleInputPassword1"
                                placeholder="MIKI NAGA">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">EXPIRATION MONTH</label>
                            <input type="month" class="form-control" id="exampleInputPassword1">

                            <label for="exampleInputPassword1">EXPIRATION YEAR</label>
                            <input type="year" class="form-control" id="exampleInputPassword1" placeholder="2021">
                        </div>
                    </div>
                    <!-- /.card-body -->
                </form>
            </div>
        </div>
        <!-- /.col -->
    </div>
    <div class="row">
        <div class="col-6">
            <p class="lead">Amount Due 2/22/2014</p>

            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th style="width:50%">Subtotal:</th>
                        <td>$250.30</td>
                    </tr>
                    <tr>
                        <th>Shipping:</th>
                        <td>$5.80</td>
                    </tr>
                    <tr>
                        <th>Total:</th>
                        <td>$265.24</td>
                    </tr>
                </table>
            </div>
        </div>
        <!-- /.col -->
    </div>
</div>
<script type="text/javascript">
    window.addEventListener("load", window.print());
</script>
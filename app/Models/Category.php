<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $fillable = ['name', 'description'];

    public function parents() {
        return $this->morphedByMany('App\Models\Category', "categorizable");
    }

    public function products() {
        return $this->morphedByMany('App\Models\Product', "categorizable");
    }
}

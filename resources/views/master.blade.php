<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>{{__('E-commerce | BiTriBie')}}</title>

  @stack('css-plugins')
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('/dashboard/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('/dashboard/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  {{-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> --}}
  <!--new style-->

  {{-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Mukta:300,400,700"> --}}
  <link rel="stylesheet" href="{{asset('fonts/icomoon/style.css')}}">
  <link rel="stylesheet" href="{{asset('/css/style.css')}}">
  <link rel="stylesheet" href="{{asset('/css/magnific-popup.css')}}">
  <link rel="stylesheet" href="{{asset('/css/jquery-ui.css')}}">
  <link rel="stylesheet" href="{{asset('/css/aos.css')}}">
  <link rel="stylesheet" href="{{asset('/css/owl.carousel.min.css')}}">
  <link rel="stylesheet" href="{{asset('/css/owl.theme.default.min.css')}}">
</head>

<body class="hold-transition layout-top-nav bg-light" style="font-family: 'Barlow'; color: #060606">
  <div class="wrapper">

    <!-- Navbar -->
    @include('layouts.master.navbar')

    <!-- /.navbar -->

    {{-- @include('layouts.master.carousel') --}}

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      @yield('content')
    </div>
    <!-- /.content-wrapper -->

    <!-- Main Footer -->
    @include('layouts.master.footer')

  </div>
  <!-- ./wrapper -->

  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="{{asset('/dashboard/plugins/jquery/jquery.min.js')}}"></script>
  <!-- Bootstrap 4 -->
  <script src="{{asset('/dashboard/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <!-- AdminLTE App -->
  <script src="{{asset('/dashboard/dist/js/adminlte.min.js')}}"></script>

  <script src="{{asset('/js/jquery-ui.js')}}"></script>
  <script src="{{asset('/js/popper.min.js')}}"></script>
  <script src="{{asset('/js/owl.carousel.min.js')}}"></script>
  <script src="{{asset('/js/jquery.magnific-popup.min.js')}}"></script>
  <script src="{{asset('/js/aos.js')}}"></script>
  <script src="{{asset('/js/main.js')}}"></script>
  <script>
    $(document).ready(() => {
        $('.main_categories').hover(() => {
            $('.category_title').hide();
            $('.sub_categories').show();
        });

        $('.main_categories').mouseleave(() => {
            $('.sub_categories').hide();
            $('.category_title').show();
        });

        $(".owl-carousel").owlCarousel();
    })
  </script>

  @stack('addonscript')
</body>

</html>

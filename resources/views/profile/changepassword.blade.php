@extends('master')

@section('content')
<div class="site-section">
	<div class="container h-100">
		<div class="row justify-content-md-center h-100">
			<div class="card-wrapper col-md-5">
				<div class="card card-primary card-outline">
					<div class="card-body box-profile">
						@if ($errors->any())
						<div class="alert alert-danger">
							<strong>{{__('Warning!')}}</strong>{{__(' Please check your input code')}}<br><br>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif

						<form role="form" action="{{route('profile.updatePassword')}}" method="post" enctype="multipart/form-data">
							{{method_field('PUT')}}
							{{csrf_field()}}
							<div class="text-center" id="edit">
								<label for="name">{{__('Change Password')}}</label>
							</div>

							<ul class="list-group list-group-unbordered mb-3">
								<li class="list-group-item">
									<div class="form-group">
										<label for="current-password">{{__('Current Password')}}</label>
										<input type="password" class="form-control" name="current-password">
									</div>
								</li>
								<li class="list-group-item">
									<div class="form-group">
										<label for="new-password">{{__('New Password')}}</label>
										<input type="password" class="form-control" name="password">
									</div>
								</li>
								<li class="list-group-item">
									<div class="form-group">
										<label for="password_confirmation">{{__('Confirm')}}</label>
										<input type="password" class="form-control" name="password_confirmation">
									</div>
								</li>
							</ul>

							<button type="submit" class="btn btn-primary btn-block"><b>{{__('Change')}}</b></button>
							<button onclick="location.href='{{ route('profile.index') }}'" type="reset"
								class="btn btn-outline-primary btn-block"><b>{{__('Cancel')}}</b></button>
						</form>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('addonscript')
<script>
	$(document).ready(() => {
        $('#edit').click(() => {
            $('#imgprofile').click();
        });
    })
</script>
@endpush
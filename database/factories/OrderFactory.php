<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        'order_date'=>$faker->date,
        'quantity'=>$faker->randomDigitNot(0),
        'description'=>$faker->paragraph,
        'status_id'=>$faker->randomDigitNot(0),
];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Cart;
use Faker\Generator as Faker;

$factory->define(Cart::class, function (Faker $faker) {
    return [
        'quantity'=>$faker->randomDigitNot(0),
        'product_detail_id'=>$faker->randomDigitNot(0),
        'description'=>$faker->paragraph,
    ];
});

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    protected $fillable = ['order_date', 'quantity', 'description', 'status_id'];

    public function cartable() {
        return $this->morphToMany('App\Models\Cart', "cartable");
    }
}

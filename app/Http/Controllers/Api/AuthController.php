<?php
namespace App\Http\Controllers\Api;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User;
use Illuminate\Support\Facades\Auth; 
use Validator;

class AuthController extends Controller 
{
    public $successStatus = 200;
    
    public function register(Request $request) {
        
        $validator = Validator::make(
            $request->all(), 
            [
                'name' => 'required', 
                'email' => 'required|email', 
                'password' => 'required', 
                'c_password' => 'required|same:password'
                ]
        );

        if ($validator->fails()) {
            return response(['error' => $validator->errors()], 401);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success['api_token'] = $user->createToken('Ecommerce')->accessToken;

        return response()->json(['data' => $success], $this->successStatus);
    }
    public function login(Request $request) {

        $login = $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if(!Auth::attempt($login)) {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
        
        $user = Auth::user();
        $success['api_token'] = $user->createToken('Ecommerce')->accessToken;

        return response()->json(['data' => $success], $this->successStatus);
    }
    public function user() {
        $user = Auth::user();
        return response()->json(['data' => $user], $this->successStatus); 
    }
}
@extends('master')

@section('content-header')
    @include('product.detail.detail-content-header')
@endsection

@section('content')
    @include('product.detail.detail-content')
@endsection
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserPayment extends Model
{
    use SoftDeletes;
    protected $fillable = ['cardholdername','credit_card_number','expiredate','cvv','remark', 'user_id'];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}

@extends('master')

@section('content')
<div class="site-section">
  <div class="container">
    <!-- Content Header (Page header) -->
    <!-- Table row -->
    <h2>PAYMENT SUMMARY</h2>
    <div class="row">
      <div class="col-12 table-responsive">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>Qty</th>
              <th>Product</th>
              <th>Price</th>
            </tr>
          </thead>
          @foreach (Auth::user()->carts as $cart)
          <tbody>
            <tr>
              <td>{{$cart->quantity}}</td>
              <td>{{$cart->productDetail->product_detail_code}}</td>
             <td>{{$cart->productDetail->price * $cart->quantity}}</td>
            </tr>  
          </tbody>
          @endforeach
        </table>
      </div>

    </div>
    <div class="row">
      <div class="col-12">
        <p class="lead"></p>

        <div class="table-responsive">
          <table class="table">
            <tr>
              <th style="width:50%">Subtotal:</th>
              <td>{{$cart->productDetail->price * $cart->quantity}}</td>
            </tr>
            <tr>
              <th>Total:</th>
              <td>{{$cart->productDetail->price * $cart->quantity}}</td>
            </tr>
          </table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    @if(count($errors) > 0)
    <div class="alert alert-danger">
     <ul>
     @foreach($errors->all() as $error)
      <li>{{$error}}</li>
     @endforeach
     </ul>
    </div>
    @endif
    <form method="post" action="paymentinfo">
      @csrf
      <div class="card">
        <div class="card-body">
          <div class="form-group">
            <div class="form-group">
              <label>Select Payment Method</label>
              <select class="form-control" name="remark">
                <option value="0">Cash on Delivery</option>
                <option value="1">Credit Card</option>
                <option value="2">Paypal</option>
              </select>
            </div>
            <div id="paypal-button"></div>
            <label for="code">{{__('Name on Card')}}</label>
            <input type="text" class="form-control" name="cardholdername" placeholder={{__("CardName")}}>
          </div>
          <div class="form-group 2">
            <label for="name">{{__('Card Number')}}</label>
            <input type="text" class="form-control" name="credit_card_number" placeholder={{__("CardNumber")}}>
          </div>
          <div class="row">
            <div class="col-3">
              <input type="text" class="form-control" name="expiredate" placeholder="Expire Date">
            </div>
            <div class="col-4">
              <input type="text" class="form-control"name="cvv" placeholder="CVV">
            </div>
          </div>
        </div>
        <div class="card-footer">
          <button type="submit" class="btn btn-info float-right">Apply</button>
        </div>
      </div>
   
    </form>

  </div>
</div>
@push('addonscript')
<script src="https://www.paypalobjects.com/api/checkout.js"></script>
<script>
  paypal.Button.render({
        env: 'sandbox', // Or 'production'
        style: {
          size: 'large',
          color: 'gold',
          shape: 'pill',
        },
        // Set up the payment:
        // 1. Add a payment callback
        payment: function(data, actions) {
          // 2. Make a request to your server
          return actions.request.post('/api/create-payment')
            .then(function(res) {
              // 3. Return res.id from the response
              // console.log(res);
              return res.id;
            });
        },
        // Execute the payment:
        // 1. Add an onAuthorize callback
        onAuthorize: function(data, actions) {
          // 2. Make a request to your server
          return actions.request.post('/api/execute-payment', {
            paymentID: data.paymentID,
            payerID:   data.payerID
          })
            .then(function(res) {
              console.log(res);
              alert('PAYMENT WENT THROUGH!!');
              // 3. Show the buyer a confirmation message.
            });
        }
      }, '#paypal-button');
</script>

@endpush
@endsection

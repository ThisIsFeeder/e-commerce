<p align="center"><img src="https://www.puthisastra.edu.kh/wp-content/uploads/2018/08/up-logo.png" width="400"></p>

### Website Project

# E-commerce

**Instructor:** Lypengleang Seang

**Group 4:** Sovannareach Darom, Sovandevit Ros, Sengchhun Chhum, Sokheng Nom, Seyha Lum, Mengkheang Theng

**University of Puthisastra**

**CS** - Dynamic Web Development Report

*   To start our project:
    > - you need to install php 7.3^ and php extension: php-xml, php-curl
    > - cp .env.example .env (in vscode)
    > - go to .env change your DB_HOST DB_PORT DB_DATABASE DB_USERNAME DB_PASSWORD
    > - composer install
    > - php artisan key:generate
    > - php artisan migrate:fresh --seed
    > - php artisan passport:install
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductDetail extends Model
{
    use SoftDeletes;
    protected $fillable = ['product_id','product_detail_code', 'parent_id', 'option_id','option_value_id','price'];

    public function parent()
    {
        return $this->belongsTo('App\Models\ProductDetail');
    }

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

    public function option()
    {
        return $this->belongsTo('App\Models\Option');
    }

    public function optionValue()
    {
        return $this->belongsTo('App\Models\OptionValue');
    }

    public function images() {
        return $this->morphToMany('App\Models\Image', "imageable");
    }

    public function cartable() {
        return $this->morphToMany('App\Models\Cart', "cartable");
    }


}

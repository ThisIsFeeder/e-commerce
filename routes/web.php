<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// HomeRoute
Route::get('/', 'HomeController@index')->name('home');
Route::get('home', 'HomeController@index')->name('home');
//SocialLoginRoute
Route::get('login/{provider}', 'SocialController@redirectTo')->name('social');
Route::get('login/{provider}/callback', 'SocialController@handleCallback');
//OtherRoute
Route::view('about', 'aboutus.index')->name('about');
Route::view('contact', 'contact.index')->name('contact');
//ProductRoute
Route::resource('products', 'ProductController');
Route::get('products.category/{category?}', 'ProductController@productByCategory')->name('products.category');
Route::get('checkout', 'CheckoutController@index')->name('checkout');

Auth::routes();

//CartRoute
Route::resource('cart', 'CartController');
//UserInformationRoute
Route::resource('profile', 'ProfileController');
Route::get('profile.changePassword', 'ProfileController@changePassword')->name('profile.changePassword');
Route::put('profile.updatePassword', 'ProfileController@updatePassword')->name('profile.updatePassword');
Route::view('forgot-password', 'auth.reset')->name('forgot-password');
route::resource('paymentinfo', 'PaymentInfoController');

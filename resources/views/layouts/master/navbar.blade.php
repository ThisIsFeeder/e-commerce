<div class="site-navbar bg-white py-2">

  <div class="search-wrap">
    <div class="container">
      <a href="#" class="search-close js-search-close"><span class="icon-close2"></span></a>
      <form action="#" method="post">
        <input type="text" class="form-control" placeholder="Search keyword and hit enter...">
      </form>
    </div>
  </div>

  <div class="container">
    <div class="d-flex align-items-center justify-content-between">
      <div class="logo">
        <div class="site-logo">
          <a href="{{route('home')}}" class="js-logo-clone">E-commerce</a>
        </div>
      </div>
      <div class="main-nav d-none d-lg-block">
        <nav class="site-navigation text-right text-md-center" role="navigation">
          <ul class="site-menu js-clone-nav d-none d-lg-block">
            <li class="{{ Request::segment(1) === 'home' ? 'active' : null }} {{ Request::segment(1) == '' ? 'active' : null }}"><a href="{{route('home')}}">Home</a></li>
            <li class="{{ Request::segment(1) === 'products' ? 'active' : null }} {{ Request::segment(1) === 'allproducts' ? 'active' : null }}"><a href="{{route('products.index')}}">All Product</a></li>
            <li class="{{ Request::segment(1) === 'contact' ? 'active' : null }}"><a href="{{route('contact')}}">Contact</a></li>
            <li class="{{ Request::segment(1) === 'about' ? 'active' : null }}"><a href="{{route('about')}}">AboutUs</a></li>
          </ul>
        </nav>
      </div>
      <div class="icons">
        <a href="#" class="icons-btn d-inline-block js-search-open"><span class="icon-search"></span></a>
        <a href="{{ route('cart.index') }}" class="icons-btn d-inline-block bag">
          <span class="icon-shopping-bag"></span>
          {{-- <span class="number">2</span> --}}
        </a>
        @guest
        <a href="{{route('login')}}" class="icons-btn d-inline-block"><span class="icon-user-o"></span></a>
        @else
        <a class="icons-btn d-inline-block" data-toggle="dropdown"><span class="icon-user-o"></span></a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0 col-md-2">
          <div class="card card-widget widget-user mb-0 border-0">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-info">
              {{-- <h3 class="widget-user-username">{{ Auth::user()->name }}</h3>
              <h5 class="widget-user-desc">Founder & CEO</h5> --}}
            </div>
            <div class="widget-user-image">
              @if(count(Auth::user()->images)>0)
              <img class="img-circle elevation-2" src="{{ asset(Auth::user()->images[0]->path) }}" style="width: 90px; height: 90px;"
                alt="User profile picture">
              @else
              <img class="img-circle elevation-2" src="../dist/img/user1-128x128.jpg"
                alt="User profile picture">
              @endif
            </div>
            <div class="card-footer">
              <div class="row justify-content-center align-self-center px-4 mb-3">
                <h3 class="widget-user-username">{{ Auth::user()->name }}</h3>
              </div>
              <div class="row">
                <div class="col-sm-6 border-right">
                  <div class="description-block">
                    <h5 class="description-header" onclick="location.href='{{ route('profile.index') }}'">Profile</h5>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
                <div class="col-sm-6">
                  <div class="description-block">
                    <h5 class="description-header"
                      onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</h5>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                      @csrf
                    </form>
                  </div>
                  <!-- /.description-block -->
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row -->
            </div>
          </div>
        </div>
        {{-- <a href="{{route('logout')}}"onclick="event.preventDefault();document.getElementById('logout-form').submit();"
        class="icons-btn d-inline-block">
        <span class="icon-user-o"></span>
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
          @csrf
        </form> --}}
        @endguest

      </div>

    </div>
  </div>
</div>

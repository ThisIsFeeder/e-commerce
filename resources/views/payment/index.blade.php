@extends('master')

@section('content')
<div class="site-section">
  <div class="container">
    <div class="row">
      <div class="col-12">
        @if(count($errors) > 0)
        <div class="alert alert-danger">
         <ul>
         @foreach($errors->all() as $error)
          <li>{{$error}}</li>
         @endforeach
         </ul>
        </div>
        @endif
        <form method="post" action="paymentinfo">
          @csrf
          <div class="card">
            <div class="card-body">
              <div class="form-group">
                <div class="form-group">
                  <label>Select Payment Method</label>
                  <select class="form-control" name="remark">
                    <option>Cash on Delivery</option>
                    <option>Credit Card</option>
                  </select>
                </div>
                <label for="code">{{__('Name on Card')}}</label>
                <input type="text" class="form-control" name="cardholdername" placeholder={{__("CardName")}}>
              </div>
              <div class="form-group">
                <label for="name">{{__('Card Number')}}</label>
                <input type="text" class="form-control" name="credit_card_number" placeholder={{__("CardNumber")}}>
              </div>
              <div class="row">
                <div class="col-3">
                  <input type="text" class="form-control" name="expiredate" placeholder="Expire Date">
                </div>
                <div class="col-4">
                  <input type="text" class="form-control"name="cvv" placeholder="CVV">
                </div>
              </div>
            </div>
            <div class="card-footer">
              <button type="submit" class="btn btn-info float-right">Apply</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
@yield('content')
@endsection

<div class="content-wrapper col-md-8 mx-auto p-0">
    <div class="d-flex">
        @include('layouts.master.carousel')
    </div>

    <!-- Category -->
    <div class="site-section pb-0">
        <div class="container">

            <!-- Title -->
            <div class="title-section mb-5">
                <h2 class="text-uppercase"><span class="d-block">{{__('Discover')}}</span> {{__('The Collections')}}</h2>
            </div>

            <!-- items -->
            <div class="row align-items-stretch">
                <div class="main_categories col-lg-6 col-md-6 mb-4">
                    <div class="bg-white d-flex align-items-center"
                        style="height: 140px; background: url('https://i.ytimg.com/vi/2plos3DB2sI/maxresdefault.jpg'); background-size: contain; background-repeat: no-repeat;">
                        <h4 class="category_title w-50 mx-auto text-right" style="display: block;">MEN</h4>
                        <div class="sub_categories position-relative ml-auto mr-5" style="display: none">
                            <ul class="row list-unstyled m-0">
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['men'])}}">Shirts</a></li>
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['men'])}}">Jean</a></li>
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['men'])}}">Scarf</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="main_categories col-lg-6 col-md-6 mb-4">
                    <div class="bg-white d-flex align-items-center"
                        style="height: 140px; background: url('https://www.styleinmood.com/wp-content/uploads/2019/08/Most-Popular-Women-Fashion-Accessories-1.jpg'); background-size: contain; background-repeat: no-repeat;">
                        <h4 class="category_title w-50 mx-auto text-right" style="display: block;">WOMEN</h4>
                        <div class="sub_categories position-relative ml-auto mr-5" style="display: none">
                            <ul class="row list-unstyled m-0">
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['women'])}}">Shirts</a></li>
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['women'])}}">Jean</a></li>
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['women'])}}">Scarf</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="main_categories col-lg-6 col-md-6 mb-4">
                    <div class="bg-white d-flex align-items-center"
                        style="height: 140px; background: url('https://redtag-stores.com/en/wp-content/uploads/boys-fashion-denim-01-min.jpg'); background-size: contain; background-repeat: no-repeat;">
                        <h4 class="category_title w-50 mx-auto text-right" style="display: block;">BOY</h4>
                        <div class="sub_categories position-relative ml-auto mr-5" style="display: none">
                            <ul class="row list-unstyled m-0">
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['boys'])}}">Shirts</a></li>
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['boys'])}}">Jean</a></li>
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['boys'])}}">Scarf</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="main_categories col-lg-6 col-md-6 mb-4">
                    <div class="bg-white d-flex align-items-center"
                        style="height: 140px; background: url('https://i.ytimg.com/vi/I29t2q10UgM/maxresdefault.jpg'); background-size: contain; background-repeat: no-repeat;">
                        <h4 class="category_title w-50 mx-auto text-right" style="display: block;">GIRL</h4>
                        <div class="sub_categories position-relative ml-auto mr-5" style="display: none">
                            <ul class="row list-unstyled m-0">
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['girls'])}}">Shirts</a></li>
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['girls'])}}">Jean</a></li>
                                <li class="sub_category p-2 px-3 font-weight-bold"><a href="{{route('products.category', ['girls'])}}">Scarf</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Main content -->
    <div class=" site-section">
        <div class="container">

            {{-- Title --}}
            <div class="row">
                <div class="title-section mb-5 col-12">
                    <h2 class="text-uppercase">{{__('Products')}}</h2>
                </div>
            </div>

            {{-- Items --}}
            <div class="row">

                @for ($index=0; $index<4; $index++) <div class="col-lg-3 col-md-4 col-sm-2 item-entry mb-4">
                    <a href="{{route('products.show', $products[$index])}}" class="product-item sm-height bg-gray d-block">
                        <img class="img-fluid"
                            src="{{asset($products[$index]->images[0]->path ?? $defaultProductImagePath)}}" />
                    </a>
                    <h2 class="item-title"><a href="{{route('products.show', $products[$index])}}">{{ $products[$index]['name'] }}</a></h2>
                    <strong class="item-price">{{ $products[$index]->productDetail()->min('price') }}</strong>
                    <div class="star-rating">
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                        <span class="icon-star2 text-warning"></span>
                    </div>
            </div>
            @endfor

        </div>

    </div>
</div>
</div>

@if (count($carts)>0)

<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="title-section text-center mb-5 col-12">
                <h2 class="text-uppercase">{{__('Best Selling')}}</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 block-3">
                <div class="nonloop-block-3 owl-carousel">
                    @for ($index=0; $index<5; $index++) <div class="item">
                        <div class="item-entry">
                            <a href="{{route('products.show', $carts[$index]->productDetail->product)}}" class="product-item md-height bg-gray d-block">
                                <img class="img-fluid"
                                    src="{{asset($carts[$index]->productDetail->images[0]->path ?? $defaultProductImagePath)}}" />
                            </a>
                            <h2 class="item-title"><a href="{{route('products.show', $carts[$index]->productDetail->product)}}">{{ $carts[$index]->productDetail->product->name }}</a>
                            </h2>
                            <strong class="item-price">{{ $carts[$index]->productDetail->price }}</strong>
                            <div class="star-rating">
                                <span class="icon-star2 text-warning"></span>
                                <span class="icon-star2 text-warning"></span>
                                <span class="icon-star2 text-warning"></span>
                                <span class="icon-star2 text-warning"></span>
                                <span class="icon-star2 text-warning"></span>
                            </div>
                        </div>
                </div>
                @endfor

            </div>
        </div>
    </div>
</div>
</div>
@endif

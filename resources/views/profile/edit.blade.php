@extends('master')

@section('content')
<div class="site-section">
	<div class="container h-100">
		<div class="row justify-content-md-center h-100">
			<div class="card-wrapper col-md-5">
				<div class="card card-primary card-outline">
					<div class="card-body box-profile">
						@if ($errors->any())
						<div class="alert alert-danger">
							<strong>{{__('Warning!')}}</strong>{{__(' Please check your input code')}}<br><br>
							<ul>
								@foreach ($errors->all() as $error)
								<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
						@endif

						<form role="form" action="{{route('profile.update', Auth::user()->id)}}" method="post"
							enctype="multipart/form-data">
							{{method_field('PUT')}}
							{{csrf_field()}}
							<div class="text-center">
								@if(count(Auth::user()->images)>0)
								<img class="profile-user-img img-fluid img-circle" style="width:100px; height: 100px;"
									src="{{ asset(Auth::user()->images[0]->path) }}" alt="User profile picture">
								@else
								<img class="profile-user-img img-fluid img-circle" style="width:100px; height: 100px;"
									src="../../dist/img/user4-128x128.jpg" alt="User profile picture">
								@endif
								<input type="file" name="imgprofile" id="imgprofile" class="form-control d-none">
							</div>
							<div class="text-center" id="edit">
								<label for="name">{{__('Edit')}}</label>
							</div>

							<ul class="list-group list-group-unbordered mb-3">
								<li class="list-group-item">
									<div class="form-group">
										<label for="name">{{__('Name')}}</label>
										<input type="text" class="form-control" name="name"
											value={{ Auth::user()->name }}>
									</div>
								</li>
								<li class="list-group-item">
									<div class="form-group">
										<label for="email">{{__('Email')}}</label>
										<input type="text" class="form-control" name="email"
											value={{ Auth::user()->email }}>
									</div>
								</li>
								<li class="list-group-item">
									<div class="form-group">
										<label for="phone">{{__('Phone Number')}}</label>
										<input type="text" class="form-control" name="phone"
											value={{ Auth::user()->phone }}>
									</div>
								</li>
							</ul>

							<button type="submit" class="btn btn-primary btn-block"><b>{{__('Update')}}</b></button>
							<button onclick="location.href='{{ route('profile.index') }}'" type="reset"
								class="btn btn-outline-primary btn-block"><b>{{__('Cancel')}}</b></button>
						</form>
					</div>
					<!-- /.card-body -->
				</div>
			</div>
		</div>
	</div>
</div>

@endsection

@push('addonscript')
<script>
	$(document).ready(() => {
        $('#edit').click(() => {
            $('#imgprofile').click();
        });
    })
</script>
@endpush
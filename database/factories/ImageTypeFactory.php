<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ImageType;
use Faker\Generator as Faker;

$factory->define(ImageType::class, function (Faker $faker) {
    return [

        'name'=>$faker->word,
        'description'=>$faker->paragraph,
        'measure'=>'10;20',
    ];
});

<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\ProductCodeType;
use Faker\Generator as Faker;

$factory->define(ProductCodeType::class, function (Faker $faker) {
    return [

        'code_type'=>$faker->sentence,
        'description'=>$faker->paragraph,
    ];
});

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Payment extends Model
{
    use SoftDeletes;
    protected $fillable = ['date', 'amount', 'invoice_id', 'user_payment_id', 'payment_method_id'];

    public function invoice()
    {
        return $this->belongsTo('App\Models\Invoice');
    }

    public function userPayment()
    {
        return $this->belongsTo('App\Models\UserPayment');
    }
}

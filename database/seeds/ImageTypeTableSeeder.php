<?php

use App\Models\ImageType;
use Illuminate\Database\Seeder;

class ImageTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(ImageType::class, 20)->create();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCodeType extends Model
{
    protected $fillable = ['code_type', 'description'];
}
